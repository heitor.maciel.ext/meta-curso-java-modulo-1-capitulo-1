package br.com.meta.java.modulo1.capitulo16;

public class TesteEnum {

  public static void main(String[] args) {
    DiasSemana dia = DiasSemana.QUINTA;
    
    System.out.println("O dia da semana é: " + dia.toString() + " | O código da semana é: " + dia.getCodigoSemana());
    
    /**
     * Iterar um enumerador
     */
    
    DiasSemana[] diassemana = DiasSemana.values();
    
    for (DiasSemana diasemana : diassemana) {
      System.out.println("Dia da semana: " + diasemana.toString());
      System.out.println("Código da semana: " + diasemana.getCodigoSemana());
      System.out.println("--------------------------------------");
    }
    
    /**
     * Pegando uma instância de enum
     */
    
    DiasSemana diassemana2 = Enum.valueOf(DiasSemana.class, "SABADO");
    System.out.println(diassemana2.toString());
    System.out.println(diassemana2.getCodigoSemana());
    
    // Ou entao
    System.out.println(Enum.valueOf(DiasSemana.class, "SABADO").toString());
    System.out.println(Enum.valueOf(DiasSemana.class, "SABADO").getCodigoSemana());
  }
}

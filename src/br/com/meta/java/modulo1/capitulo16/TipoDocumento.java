package br.com.meta.java.modulo1.capitulo16;

public enum TipoDocumento {
  CPF {
    @Override
    public String geraNumeroDocumento() {
      return new GeraCpfCnpj().cpf();
    }

    @Override
    public int getCodigo() {
      // TODO Auto-generated method stub
      return 10;
    }
  },
  CNPJ {
    @Override
    public String geraNumeroDocumento() {
      return new GeraCpfCnpj().cnpj();
    }

    @Override
    public int getCodigo() {
      // TODO Auto-generated method stub
      return 20;
    }
  };
  
  public abstract String geraNumeroDocumento();
  public abstract int getCodigo();
  
}

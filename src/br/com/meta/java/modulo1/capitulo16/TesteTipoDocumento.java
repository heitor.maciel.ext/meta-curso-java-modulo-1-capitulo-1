package br.com.meta.java.modulo1.capitulo16;

public class TesteTipoDocumento {

  public static void main(String[] args) {
    TipoDocumento[] tiposDocumento = TipoDocumento.values();
    
    for (TipoDocumento tipoDocumento : tiposDocumento) {
      System.out.println(tipoDocumento.toString() + " - " + tipoDocumento.geraNumeroDocumento());
    }
  }

}

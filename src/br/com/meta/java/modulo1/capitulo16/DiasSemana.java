package br.com.meta.java.modulo1.capitulo16;

public enum DiasSemana {
  SEGUNDA(1),
  TERCA(2),
  QUARTA(3),
  QUINTA(4),
  SEXTA(5),
  SABADO(6),
  DOMINGO(7);
  
  private int codigo;
  
  DiasSemana(int codigo) {
    this.codigo = codigo;
  }
  
  public int getCodigoSemana() {
    return this.codigo;
  }
}

package br.com.meta.java.modulo1.capitulo23;

public class Papagaio extends Ave {

  @Override
  public void voar() {
    System.out.println("Realiza o Vôo de um papgaio");
  }

  @Override
  public void emitirSom() {
    System.out.println("Emite o som de um papagaio");
  }

}

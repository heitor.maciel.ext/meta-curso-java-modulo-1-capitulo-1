package br.com.meta.java.modulo1.capitulo23.exercicio;

public abstract class Ave extends Animal {
  public abstract void voar();
}

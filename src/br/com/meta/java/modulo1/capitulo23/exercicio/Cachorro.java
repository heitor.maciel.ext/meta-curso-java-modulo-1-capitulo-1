package br.com.meta.java.modulo1.capitulo23.exercicio;

public class Cachorro extends Mamifero {
  String tamanho;
  String raca;
  
  public String getRaca() {
    return raca;
  }
  public void setRaca(String raca) {
    this.raca = raca;
  }
  public String getTamanho() {
    return tamanho;
  }
  public void setTamanho(String tamanho) {
    this.tamanho = tamanho;
  }
  @Override
  public void amamentar() {
    System.out.println("Amamentação do cachorro");
  }
  @Override
  public void emitirSom() {
    System.out.println("Au au");
  }
  
  
}

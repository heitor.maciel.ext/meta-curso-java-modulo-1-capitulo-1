package br.com.meta.java.modulo1.capitulo23.exercicio;

public abstract class Mamifero extends Animal {
  public abstract void amamentar();
}

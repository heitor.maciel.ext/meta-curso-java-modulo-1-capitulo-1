package br.com.meta.java.modulo1.capitulo23;

public abstract class Mamifero extends Animal {
  public abstract void amamentar();
}

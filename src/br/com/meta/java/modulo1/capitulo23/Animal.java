package br.com.meta.java.modulo1.capitulo23;

public abstract class Animal {
  private String nome;
  
  public Animal() {
    
  }
  
  public String getNome() {
    return "Heitor";
  }

  public void setNome(String nome) {
    this.nome = nome;
  }
  
  /**
   * Métodos abstratos
   */
  public abstract void emitirSom();
}

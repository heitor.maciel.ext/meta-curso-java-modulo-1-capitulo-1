package br.com.meta.java.modulo1.capitulo23;

public class Gato extends Mamifero {
  private String raca;

  public String getRaca() {
    return raca;
  }

  public void setRaca(String raca) {
    this.raca = raca;
  }

  @Override
  public void amamentar() {
    System.out.println("Realiza a amamentação de um gato");
    
  }

  @Override
  public void emitirSom() {
    System.out.println("Emite o som de um gato - MIAU");
  }

}

package br.com.meta.java.modulo1.capitulo23;

public class Cachorro extends Mamifero {
  private String tamanho;
  private String raca;
  
  public String getTamanho() {
    return tamanho;
  }
  public void setTamanho(String tamanho) {
    this.tamanho = tamanho;
  }
  
  public String getRaca() {
    return raca;
  }
  public void setRaca(String raca) {
    this.raca = raca;
  }
  
  @Override
  public void amamentar() {
    System.out.println("Realiza amamamentação do cachorro");
  }
  
  @Override
  public void emitirSom() {
    System.out.println("Au au");
  }
}

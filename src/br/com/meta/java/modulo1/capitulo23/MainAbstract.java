package br.com.meta.java.modulo1.capitulo23;

public class MainAbstract {

  public static void main(String[] args) {
    Papagaio papagaio = new Papagaio();
    papagaio.emitirSom();
    papagaio.voar();
    
    Gato gato = new Gato();
    gato.emitirSom();
    gato.amamentar();
    
    Cachorro cachorro = new Cachorro();
    cachorro.emitirSom();
    cachorro.amamentar();
    
    //Mamifero mamifero = new Mamifero();
    //Ave ave = new Ave();
  }
}

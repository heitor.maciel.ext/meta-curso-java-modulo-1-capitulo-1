package br.com.meta.java.modulo1.capitulo11;

public class Array {

  public static void main(String[] args) {
    // Digamos que é necessário pegar todas as maiores velocidades máximas 
    // de um caminhão da Sascar na semana e verificar qual delas foi a maior de todas
    double velocidadeDia1 = 157.00;
    double velocidadeDia2 = 150.00;
    double velocidadeDia3 = 169.00;
    double velocidadeDia4 = 151.00;
    double velocidadeDia5 = 130.00;
    double velocidadeDia6 = 290.00;
    double velocidadeDia7 = 300.00;
    
    // Não fazer desta forma, pois existem arrays
    double velocidade[];
    
    double[] velocidades = new double[6];
    velocidades[0] = 157.00;
    velocidades[1] = 150.00;
    velocidades[2] = 169.00;
    velocidades[3] = 151.00;
    velocidades[4] = 130.00;
    velocidades[5] = 290.00;
    
    System.out.println("O valor da velocidade do dia 1: " + velocidades[0]);
    System.out.println("O valor da velocidade do dia 2: " + velocidades[1]);
    System.out.println("O valor da velocidade do dia 3: " + velocidades[2]);
    System.out.println("O valor da velocidade do dia 4: " + velocidades[3]);
    System.out.println("O valor da velocidade do dia 5: " + velocidades[4]);
    System.out.println("O valor da velocidade do dia 6: " + velocidades[5]);  
    
    // Tamanho do array
    System.out.println("O tamanho do array: " + velocidades.length);
    
    // Endereço de memória
    System.out.println("O endereço de memória: " + velocidades);
    
    // Iteração em cima de um array
    for (int i = 0; i < velocidades.length; i++) {
      System.out.println("A velocidade do dia " + (i + 1) + " é : " + velocidades[i]);
    }
    
    System.out.println("");
    
    // Foreach - Versão 5 >= Java
    int i = 0;
    for (double velo : velocidades) {
      System.out.println("A velocidade do dia " + (i + 1) + " é : " + velo);
      i++;
    }
  }
}

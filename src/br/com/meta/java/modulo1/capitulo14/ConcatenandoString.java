package br.com.meta.java.modulo1.capitulo14;

public class ConcatenandoString {

  public static void main(String[] args) {
    String part1 = "Segunda";
    String part2 = "Terça";
    String part3 = "Quarta";
    String part4 = "Quinta";
    String part5 = "Sexta";
    String part6 = "Sábado";
    String part7 = "Domingo";
    
    System.out.println(
      part1 + System.lineSeparator() +
      part2 + System.lineSeparator() +
      part3 + System.lineSeparator() +
      part4 + System.lineSeparator() +
      part5 + System.lineSeparator() +
      part6 + System.lineSeparator() +
      part7
    );
    
    System.out.println("");
    System.out.println("");
    
    System.out.println(
        part1 + System.getProperty("line.separator") +
        part2 + System.getProperty("line.separator") +
        part3 + System.getProperty("line.separator") +
        part4 + System.getProperty("line.separator") +
        part5 + System.getProperty("line.separator") +
        part6 + System.getProperty("line.separator") +
        part7
      );
    
    System.out.println("");
    System.out.println("");
    
    System.out.println(
        part1 + "\r\n" +
        part2 + "\r\n" +
        part3 + "\r\n" +
        part4 + "\r\n" +
        part5 + "\r\n" +
        part6 + "\r\n" +
        part7
      );
    
    System.out.println("");
    System.out.println("");
    
    System.out.println("Bem vindo a Sascar!\n\n" +
        "Um exemplo de concatenação de strings com quebra de linha.\n" +
        "Qualquer dúvida só falar. " +
        "Att, Heitor Maciel" );
  }
}

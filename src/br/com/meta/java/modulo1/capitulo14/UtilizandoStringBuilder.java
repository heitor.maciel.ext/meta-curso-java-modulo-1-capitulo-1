package br.com.meta.java.modulo1.capitulo14;

public class UtilizandoStringBuilder {

  public static void main(String[] args) {
    StringBuilder concat = new StringBuilder();
    concat.append("Primeira String");
    concat.append("Segunda String");
    concat.append("Terceira String");
    concat.append("Quarta String");
    concat.append("Quinta String");
    
    System.out.println(concat.toString());
    //System.out.println(concat);
    
    StringBuilder concat2 = new StringBuilder();
    concat2.append("Primeira String");
    concat2.append(System.lineSeparator());
    concat2.append("Segunda String");
    concat2.append(System.lineSeparator());
    concat2.append("Terceira String");
    concat2.append(System.lineSeparator());
    concat2.append("Quarta String");
    concat2.append(System.lineSeparator());
    concat2.append("Quinta String");
    
    System.out.println(concat2.toString());
  }
}

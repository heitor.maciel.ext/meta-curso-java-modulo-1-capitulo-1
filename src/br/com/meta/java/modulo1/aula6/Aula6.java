package br.com.meta.java.modulo1.aula6;

public class Aula6 {
  /**
   * Controle de decisão
   * @param args
   */
  public static void main(String[] args) {
    boolean existeOutraForma = true;
    
    if (existeOutraForma)
      System.out.println("Sim, existe outra forma");
      System.out.println("Heitor");
    
    if (existeOutraForma) {
      System.out.println("Sim, existe outra forma");
    }
    
    int idade = 15;
    
    if (idade > 18)
      System.out.println("É maior de idade");
    else
      System.out.println("É menor de idade");
    
    String genero = "MASCULINO";
    
    if (idade > 18 && genero.equals("MASCULINO")) {
      System.out.println("É maior de idade e o sexo é masculino");
    }
    else if (idade < 18)
      System.out.println("É menor de idade");
    else if (!genero.equals("MASCULINO"))
      System.out.println("Não é do sexo masculino");
    
  }

}

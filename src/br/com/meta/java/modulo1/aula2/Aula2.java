package br.com.meta.java.modulo1.aula2;

public class Aula2 {
  
  /**
   * @param args
   * Passando argumentos por parâmetro
   */
  public static void main(String[] args) {
    System.out.println("Voce digitou " + args[0]);
  }
  
  /**
   * Comando:
   * java Aula2.java "Texto que voce digitou"
   */
}

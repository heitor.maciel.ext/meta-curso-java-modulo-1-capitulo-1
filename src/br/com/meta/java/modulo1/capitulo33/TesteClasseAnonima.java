package br.com.meta.java.modulo1.capitulo33;

public class TesteClasseAnonima {

	public static void main(String[] args) {
		Anonima anonima1 = new Anonima();
		
		anonima1.imprimirTexto();
		
		Anonima anonima = new Anonima() {
			public void imprimirTexto() {
				System.out.println("Mudando o texto da classe");
			}
		};
		
		anonima.imprimirTexto();
	}

}

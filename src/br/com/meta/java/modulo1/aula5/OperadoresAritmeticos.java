package br.com.meta.java.modulo1.aula5;

public class OperadoresAritmeticos {

  public static void main(String[] args) {
    int resultadoSoma = 20 + 40;
    System.out.println("O resultado da soma foi: " + resultadoSoma);
    
    int resultadoSubtracao = 100 - 10;
    System.out.println("O resultado da substracao foi: " + resultadoSubtracao);
    
    int resultadoMultiplicacao = 200 * 25;
    System.out.println("O resultado da multiplicacao foi: " + resultadoMultiplicacao);
    
    int resultadoDivisao = 100 / 25;
    System.out.println("O resultado da divisao foi: " + resultadoDivisao);
    
    /* Exemplo para saber o resto de uma visiao */
    int resultadoResto = 100 % 7;
    System.out.println("O resto da divisão é: " + resultadoResto);
    
    /* Observação */
    /* O operador aritmético + também tem a função de concatenar strings */
    String primeiroNome = "Heitor";
    String segundoNome = "Maciel";
    
    System.out.println("O nome é: " + primeiroNome + " " + segundoNome);
    
    /* Também é possível realizar uma adição e substração através de uma boa prática de iteração */
    /* Exemplo muito usado em loops */
    int variavel1 = 500;
    variavel1++;
    System.out.println("variavel1: " + variavel1);
    
    int variavel2 = 500;
    variavel2--;
    System.out.println("variavel2: " + variavel2);
    
  }
}

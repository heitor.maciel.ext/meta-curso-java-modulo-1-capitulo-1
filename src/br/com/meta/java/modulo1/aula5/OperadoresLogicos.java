package br.com.meta.java.modulo1.aula5;

public class OperadoresLogicos {

  public static void main(String[] args) {
    // TODO Auto-generated method stub
    
    /**
     * Operador AND -> &&
     * Operador OR  -> ||
     * Operador NOT -> !
     * Operador XOR -> ^ -> OU EXCLUSIVO
     */
    
    int valor1 = 10;
    int valor2 = 20;
    
    if (valor1 < 100 && valor2 < 100) 
      System.out.println("O valor 1 é menor que 100 e o valor 2 é menor que 100");
    
    int valor3 = 50;
    int valor4 = 100;
    
    if (valor3 == 50 || valor4 == 90)
      System.out.println("Entrou na condição");
    
    int valor5 = 70;
    
    if (!(valor5 == 70))
      System.out.println("O valor 5 é diferente de 70");
    
    int valor6 = 50;
    int valor7 = 100;
    
    if (valor6 == 50 ^ valor7 == 100)
      System.out.println("Entrou na condição XOR");
  }

}

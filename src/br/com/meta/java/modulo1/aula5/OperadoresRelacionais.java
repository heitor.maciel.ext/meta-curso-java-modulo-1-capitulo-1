package br.com.meta.java.modulo1.aula5;

public class OperadoresRelacionais {

  public static void main(String[] args) {
    int valor1 = 1;
    int valor2 = 10;
    
    if (valor1 == valor2)
      System.out.println("os valores são iguais");
    
    if (valor1 != valor2)
      System.out.println("Os valores são diferentes");
    
    if (valor1 > valor2)
      System.out.println("O valor 1 é maior que ao valor 2");
    
    if (valor1 >= valor2)
      System.out.println("O valor 1 é maior ou igual que o valor 2");
    
    if (valor1 < valor2)
      System.out.println("O valor 1 é menor que o valor 2");
    
    if (valor1 <= valor2)
      System.out.println("O valor 1 é menor ou igual que o valor 2");
    
    String nome = "HEITOR";
    
    if (nome.equals("HEITOR"))
      System.out.println("É o Heitor");
    else
      System.out.println("Não é o Heitor");
  }

}

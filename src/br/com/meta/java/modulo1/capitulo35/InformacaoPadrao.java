package br.com.meta.java.modulo1.capitulo35;

public @interface InformacaoPadrao {
	String nomeMetodo();
	int qtdParcelas();
	String nomeEmpresa() default "Sascar Tecnologia";
	
	/**
	 * Mostrar exemplos em Spring Boot
	 */
}

package br.com.meta.java.modulo1.capitulo28;

import br.com.meta.java.modulo1.capitulo28.exception.CustomerException;
import br.com.meta.java.modulo1.capitulo28.exception.ExceptionAulaJava;

public class TesteMyException {

  public static void main(String[] args) throws ExceptionAulaJava {
    Customer customer = new Customer();
    
    try {
      customer.update();
    } 
    catch (CustomerException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

}

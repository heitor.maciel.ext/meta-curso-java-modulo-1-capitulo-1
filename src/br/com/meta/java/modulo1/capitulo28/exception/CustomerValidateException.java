package br.com.meta.java.modulo1.capitulo28.exception;

public class CustomerValidateException extends CustomerException {
  Integer codigo;
  String mensagem;
  
  public CustomerValidateException() {
    super();
  }
  
  public CustomerValidateException(Integer Codigo, String Mensagem) {
    super();
    this.codigo = Codigo;
    this.mensagem = Mensagem;
  }

  public Integer getCodigo() {
    return codigo;
  }

  public void setCodigo(Integer codigo) {
    this.codigo = codigo;
  }

  public String getMensagem() {
    return mensagem;
  }

  public void setMensagem(String mensagem) {
    this.mensagem = mensagem;
  }
  
  @Override
  public String toString() {
    StringBuilder concat = new StringBuilder();
    concat.append("Erro " + this.getClass().getName() + System.lineSeparator());
    concat.append(" - Código: " + this.codigo);
    concat.append(" - Mensagem: " + this.mensagem);
    
    return concat.toString();    
  }
}

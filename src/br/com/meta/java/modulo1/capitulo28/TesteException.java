package br.com.meta.java.modulo1.capitulo28;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import br.com.meta.java.modulo1.capitulo20.Pessoa;

public class TesteException {

  public static void main(String[] args) throws Exception {
    /**
     * https://docs.oracle.com/javase/10/docs/api/java/lang/package-tree.html
     * 
     * Throwable
     * 
     *   - Error
     *   - Exception
     *   
     *   * Qual a diferença?
     *     - Error é um erro que irá fazer com que o seu programa finalize imediatamente 
     *     - Não é possível realizar o tratamento de um Error com try catch
     *       Exemplo: OutOfMemoryError
     *       
     *     - Exception é um erro inesperado que é possível ser tratado com try catch 
     *       e seu programa não irá ser finalizado.
     *       Exemplo: NullPointerException
     *       
     *       Exceptions não verificadas - RunTimeException
     *        - Acontecem em tempo de execução
     *        - Normalmente é um bug da aplicação
     *        
     *       Exceptions verificadas
     *        - O compilador pede pra automaticamente para realizar o tratamento
     *        - Exemplo na leitura de um arquivo
     */ 
    
    //codigoComErro();
    //codigoComTratamento();
    //codigoComTratamentoMultiplo();
    //codigoComTratamentoMultiploOutraManeira();
    //codigoComTratamentoMultiploGenerico();
    //codigoTratamentoFinnaly();
    //codigoTratamentoFinnalyPegadinha();
    
    //usandoClasseException();
    
    //usandoThrows();
    exemploExcecaoVerificada();
  }
  
  public static void codigoComErro() {
    /**
     * Vou fazer um código que vai dar erro
     */
    Pessoa pessoa = null;
    
    System.out.println("Antes da Exception");
    
    pessoa.setCpf("07503436999");
    
    System.out.println("Este system.out não será impresso no console");
  }

  public static void codigoComTratamento() {
    try {
      Pessoa pessoa = null;
      
      System.out.println("Antes da Exception");
      
      pessoa.setCpf("07503436999");
      
      System.out.println("Este system.out não será impresso no console");      
    } 
    catch (Exception e) {
      System.out.println("Aconteceu um erro de NullPointerException para acessar a variável");
    }
    
    System.out.println("Aqui será impresso após o tratamento do try catch");
  }
  
  public static void codigoComTratamentoMultiplo() {
    try {
       /*Pessoa pessoa = null;
       
       System.out.println("Antes da Exception");
       
       pessoa.setCpf("07503436999");
       
       System.out.println("Este system.out não será impresso no console");*/
       
      
      int[] vetor = {1, 3, 5, 7, 9, 11};
      
      System.out.println(vetor[7]);
    } 
    catch (ArrayIndexOutOfBoundsException e) {
      System.out.println("Ocorreu uma Exception: ArrayIndexOutOfBoundsException");
    }
    catch(NullPointerException e) {
      System.out.println("Ocorreu uma Exception: NullPointerException");
    }
    
    System.out.println("Depois da Exception");
  }
  
  public static void codigoComTratamentoMultiploOutraManeira() {
    try {
       /*Pessoa pessoa = null;
       
       System.out.println("Antes da Exception");
       
       pessoa.setCpf("07503436999");
       
       System.out.println("Este system.out não será impresso no console");*/
       
      
      int[] vetor = {1, 3, 5, 7, 9, 11};
      
      System.out.println(vetor[7]);
    } 
    catch (ArrayIndexOutOfBoundsException | NullPointerException e) {
      
      if (e instanceof ArrayIndexOutOfBoundsException)
        System.out.println("Ocorreu uma Exception: ArrayIndexOutOfBoundsException");
      else if (e instanceof NullPointerException)
        System.out.println("Ocorreu uma Exception: NullPointerException");
    }
    
    System.out.println("Depois da Exception");
  }
  
  public static void codigoComTratamentoMultiploGenerico() {
    /**
     * Lembrar de colocar o Throwable na frente para dar erro
     */
    try {
       /*Pessoa pessoa = null;
       
       System.out.println("Antes da Exception");
       
       pessoa.setCpf("07503436999");
       
       System.out.println("Este system.out não será impresso no console");*/
       
      
      /*int[] vetor = {1, 3, 5, 7, 9, 11};
      
      System.out.println(vetor[7]); */
      
      Double valor = new Double(20);
      valor = (double) (0 / 0);
    }
    catch (ArrayIndexOutOfBoundsException e) {
      System.out.println("Ocorreu uma Exception: ArrayIndexOutOfBoundsException");
    }
    catch(NullPointerException e) {
      System.out.println("Ocorreu uma Exception: NullPointerException");
    }
    catch (Throwable e) {
      System.out.println("Ocorreu um erro");
    }
    
    System.out.println("Depois da Exception");
  }
  
  public static void codigoTratamentoFinnaly() {
    try {
      /*Pessoa pessoa = null;
      
      System.out.println("Antes da Exception");
      
      pessoa.setCpf("07503436999");
      
      System.out.println("Este system.out não será impresso no console");*/
      
     
     int[] vetor = {1, 3, 5, 7, 9, 11};
     
     System.out.println(vetor[7]);
   } 
   catch (ArrayIndexOutOfBoundsException | NullPointerException e) {
     if (e instanceof ArrayIndexOutOfBoundsException)
       System.out.println("Ocorreu uma Exception: ArrayIndexOutOfBoundsException");
     else if (e instanceof NullPointerException)
       System.out.println("Ocorreu uma Exception: NullPointerException");
   }
   finally {
     System.out.println("Este código sempre vai executar");
   }
  }
  
  public static void codigoTratamentoFinnalyPegadinha() {
    /**
     * Qual a saída desse código
     */
    
    try {
      Pessoa pessoa = null;
      
      System.out.println("Antes da Exception");
      
      pessoa.setCpf("07503436999");
      
      System.out.println("Este system.out não será impresso no console");
     
     /*int[] vetor = {1, 3, 5, 7, 9, 11};
     
     System.out.println(vetor[7]); */
   } 
   catch (ArrayIndexOutOfBoundsException e) {
     System.out.println("Ocorreu uma Exception: ArrayIndexOutOfBoundsException");
     System.exit(0);
   }
   catch(NullPointerException e) {
     System.out.println("Ocorreu uma Exception: NullPointerException");
     System.exit(0);
   }
   finally {
     System.out.println("Este código será executado");
   }
  }
  
  public static void usandoClasseException() {
    /**
     * Qual a saída desse código
     */
    
    try {
      Pessoa pessoa = null;
      
      System.out.println("Antes da Exception");
      
      pessoa.setCpf("07503436999");
      
      System.out.println("Este system.out não será impresso no console");
      
     
     /*int[] vetor = {1, 3, 5, 7, 9, 11};
     
     System.out.println(vetor[7]); */
   } 
   catch (Exception e) {
     System.out.println("Ocorreu uma Exception: ArrayIndexOutOfBoundsException");
     System.out.println(e.getMessage());
     e.printStackTrace();
   }
   finally {
     System.out.println("Este código será executado");
   }
  }
  
  public static void usandoThrows() throws Exception {
    Pessoa pessoa = null;
    
    System.out.println("Antes da Exception");
    
    pessoa.setCpf("07503436999");
    
    System.out.println("Este system.out não será impresso no console");    
  }
  
  public static void exemploExcecaoVerificada() {   
    FileInputStream arquivo = null;
    try {
      arquivo = new FileInputStream("/var/www/html/appconf.json");
    } 
    catch (FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    
    
    
    
    FileInputStream FIS = null;
    try {
        FIS = new FileInputStream("/var/www/html/appconf.json");
    } 
    catch (FileNotFoundException ex) {
        System.out.println("Ocorreu um erro ao verificar se o arquivo existe");
    }

    int x;
    try {
        while ((x = FIS.read()) != 0) {
          System.out.println(x);
        }
    } 
    catch (IOException ex) {
      System.out.println("Ocorreu um erro ao realizar a leitura do arquivo");
    }
  }
}

package br.com.meta.java.modulo1.capitulo28;

import br.com.meta.java.modulo1.capitulo28.exception.CustomerException;

public interface ICustomerAction {
  void insert() throws CustomerException;
  void update() throws CustomerException;
  void delete() throws CustomerException;
  void validate() throws CustomerException;
}

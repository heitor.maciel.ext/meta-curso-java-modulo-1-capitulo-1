package br.com.meta.java.modulo1.capitulo28;

import br.com.meta.java.modulo1.capitulo28.exception.CustomerDeleteException;
import br.com.meta.java.modulo1.capitulo28.exception.CustomerException;
import br.com.meta.java.modulo1.capitulo28.exception.CustomerInsertException;
import br.com.meta.java.modulo1.capitulo28.exception.CustomerUpdateException;
import br.com.meta.java.modulo1.capitulo28.exception.CustomerValidateException;
import br.com.meta.java.modulo1.capitulo28.exception.ExceptionAulaJava;

public class Customer implements ICustomerAction {
  Integer id;
  String nome;
  String sobrenome;
  Integer idade;
  
  public Customer() throws ExceptionAulaJava {

  }
  
  @Override
  public void insert() throws CustomerException {
    // TODO Auto-generated method stub
    throw new CustomerInsertException(-10, "Não foi possível inserir o registro do cliente");
  }

  @Override
  public void update() throws CustomerException {
    throw new CustomerUpdateException(-10, "Não foi possível atualizar o registro do cliente");
  }

  @Override
  public void delete() throws CustomerException {
    throw new CustomerDeleteException(-10, "Não foi possível deletar o cliente");
  }

  @Override
  public void validate() throws CustomerException {
    if (this.nome == null)
      throw new CustomerValidateException(-10, "O nome do cliente não foi preenchido");
    if (this.sobrenome == null)
      throw new CustomerValidateException(-20, "O sobrenome do cliente não foi preenchido");
    if (this.idade == null)
      throw new CustomerValidateException(-30, "A idade não foi informada");
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getSobrenome() {
    return sobrenome;
  }

  public void setSobrenome(String sobrenome) {
    this.sobrenome = sobrenome;
  }

  public Integer getIdade() {
    return idade;
  }

  public void setIdade(Integer idade) {
    this.idade = idade;
  }

}

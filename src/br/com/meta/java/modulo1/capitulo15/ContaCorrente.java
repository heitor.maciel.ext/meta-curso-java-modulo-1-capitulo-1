package br.com.meta.java.modulo1.capitulo15;

import java.math.BigDecimal;

public class ContaCorrente {
  BigDecimal saldo = new BigDecimal(0);
  String nomeCliente;
  
  public ContaCorrente() {
    // TODO Auto-generated constructor stub
  }
  
  public ContaCorrente(String nomeCliente, BigDecimal saldo) {
    this.nomeCliente = nomeCliente;
    this.saldo = saldo;
  }
  
  public BigDecimal getSaldo() {
    return this.saldo;
  }

  public void setSaldo(BigDecimal saldo) {
    this.saldo = saldo;
  }
  
  public void debitar(BigDecimal valor) {
    this.saldo = this.saldo.subtract(valor);
  }
  
  public void depositar(BigDecimal valor) {
    this.saldo = this.saldo.add(valor);
  }

  public String getNomeCliente() {
    return nomeCliente;
  }

  public void setNomeCliente(String nomeCliente) {
    this.nomeCliente = nomeCliente;
  }
}

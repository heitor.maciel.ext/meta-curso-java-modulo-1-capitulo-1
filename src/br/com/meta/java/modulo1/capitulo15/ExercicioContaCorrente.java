package br.com.meta.java.modulo1.capitulo15;

import java.math.BigDecimal;
import java.util.Scanner;

public class ExercicioContaCorrente {

  private static Scanner scan;

  public static void main(String[] args) {
    Boolean menuAtivo = true;
    scan = new Scanner(System.in);
    
    StringBuilder opcoesmenu = new StringBuilder();
    opcoesmenu.append("1 - Retirar dinheiro" + System.lineSeparator());
    opcoesmenu.append("2 - Depositar dinheiro" + System.lineSeparator());
    opcoesmenu.append("3 - Consultar saldo atual" + System.lineSeparator());
    opcoesmenu.append("9 - Sair do menu" + System.lineSeparator());
    
    System.out.println("Informe seu nome: " + System.lineSeparator());
    String nomeCliente = scan.nextLine();
    
    System.out.println("Olá, " + nomeCliente + ", seja bem vindo ao BancoSascar! " + System.lineSeparator());
    System.out.println("Informe o seu saldo inicial: ");
    
    BigDecimal saldoInicial = scan.nextBigDecimal();
    // Instancia a conta corrente já com o saldo inicial no construtor
    ContaCorrente contaCorrente = new ContaCorrente(nomeCliente, saldoInicial);
    
    System.out.println("");
    
    System.out.println("Agora, vamos as opções do menu: " + System.lineSeparator());

    while (menuAtivo) {
      System.out.println(opcoesmenu);
      Integer opcao = scan.nextInt();
      
      switch (opcao) {
      case 1:
        System.out.println("Informe o valor que será retirado da sua conta.");
        BigDecimal valorDebito = scan.nextBigDecimal();
        contaCorrente.debitar(valorDebito);
        break;
      case 2:
        System.out.println("Informe o valor que será depositado na sua conta.");
        BigDecimal valorDeposito = scan.nextBigDecimal();
        contaCorrente.depositar(valorDeposito);
        break;
      case 3:
        System.out.println("Olá " + contaCorrente.getNomeCliente() + ", seu saldo atual é: " + contaCorrente.getSaldo());
        break;
      case 9:
        menuAtivo = false;
        break;
      default:
        System.out.println("A opção escolhida é inválida, por favor selecione uma opção válida" + System.lineSeparator());
        System.out.println("");
        break;
      }
    }
    
    System.out.println("");    
    System.out.println("Muito obrigado por utilizar o banco digital Sascar");
    System.out.println("");    
  }
}

package br.com.meta.java.modulo1.capitulo19;

public class Aluno extends Pessoa {
  private String curso;
  
  public Aluno() {
    super();
  }
    
  public Aluno(String nome, String endereco, String telefone, String curso) {
    super(nome, endereco, telefone);
    this.curso = curso;
  }

  public String getCurso() {
    return curso;
  }
  
  public void setCurso(String curso) {
    this.curso = curso;
  }  
  public double calcularMedia() {
    return 0;
  }
  
  public boolean verificarAlunoAprovado() {
    return true;
  }
  
  public void setarCPFManualmente(String cpf) {
    super.setCpf(cpf);
    // ou 
    this.setCpf(cpf);
    
    super.metodoEscrever();
    // ou
    this.metodoEscrever();
  }
}

package br.com.meta.java.modulo1.aula9;

import java.util.Iterator;

public class LoopFor {

  public static void main(String[] args) {
    // For "Normal"
    
    System.out.println("------------------");
    // Incrementa
    for (int i = 0; i < 10; i++) {
      System.out.println("O valor de i: " + i);
    }
    
    System.out.println("------------------");
    
    // Decrementa
    for (int j = 5; j > 0; j--) {
      System.out.println("O valor de j: " + j);
    }
    
    System.out.println("------------------");
    
    // For com duas variáveis
    for (int i = 0, j = 10; i < j ;i++, j--) {
      System.out.println("i = " + i + "; j = " + j);
    }
    
    System.out.println("------------------");
    // variáveis ausentes
    int count = 0;
    
    for (; count < 5; count++) {
      System.out.println("O valor de count é: " + count);
    }
    
  }

}

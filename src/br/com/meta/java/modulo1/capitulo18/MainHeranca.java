package br.com.meta.java.modulo1.capitulo18;

public class MainHeranca {

  public static void main(String[] args) {
    
    /***
     * Relação de herança:
     * Tal classe É UM ou É UMA
     * Logo mais vamos explicar
     */
    
    /**
     * Pelo fato da classe Aluno herdar a classe
     * Pessoa, se realizar uma instância de Aluno
     * Iremos ter acesso aos atributos da classe Pessoa
     */
    
    Aluno aluno1 = new Aluno();
    //aluno1.set
    
    /**
     * É possível fazer assim, pelo fato 
     * da classe Pessoa ser uma superclasse de Aluno.
     * 
     * Porém, só é possível ter acesso aos atributos
     * da classe pessoa.
     */
    Pessoa aluno2 = new Aluno();
    //aluno2.set
    
    // ou então
    Pessoa professor1 = new Professor();
    //professor1.
  }
  
  private void setar(Pessoa pessoa) {
    
  }

}

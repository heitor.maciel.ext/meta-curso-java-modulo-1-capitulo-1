package br.com.meta.java.modulo1.capitulo18;

public class Aluno extends Pessoa {
  private String curso;
  
  public String getCurso() {
    return curso;
  }
  public void setCurso(String curso) {
    this.curso = curso;
  }  
  public double calcularMedia() {
    return 0;
  }
  
  public boolean verificarAlunoAprovado() {
    return true;
  }
}

package br.com.meta.java.modulo1.capitulo38;

public class TesteRunnable {

	public static void main(String[] args) {
		//ThreadRunnable runnable = new ThreadRunnable("Thread #1", 2000);
		Thread t1 = new Thread();
		//Thread thread = new Thread(runnable);
		//thread.start();
		
		ThreadRunnable runnable2 = new ThreadRunnable("Thread #2", 600);
		Thread thread2 = new Thread(runnable2);
		thread2.start();
		
		ThreadRunnable runnable3 = new ThreadRunnable("Thread #3", 100);
		Thread thread3 = new Thread(runnable3);
		thread3.start();
		
		// ou entao
		ThreadRunnable runnable = new ThreadRunnable("Thread #1", 2000);
		new Thread(runnable).start();
		
		//new Thread(new ThreadRunnable("Thread #1", 2000)).start();
		//new Thread(runnable2).start();
		//new Thread(runnable3).start();
		
	}

}

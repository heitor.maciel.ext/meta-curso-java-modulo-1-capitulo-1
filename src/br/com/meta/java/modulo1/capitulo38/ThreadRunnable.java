package br.com.meta.java.modulo1.capitulo38;

public class ThreadRunnable implements Runnable {
	private String nomeThread;
	private int tempo;
	
	public ThreadRunnable() {}
	
	public ThreadRunnable(String nomeThread, int tempo) {
		super();
		this.nomeThread = nomeThread;
		this.tempo = tempo;
	}

	@Override
	public void run() {
		
		try {
			for (int i = 0; i < 10; i++) {
				System.out.println(this.nomeThread + " executando o contador: " + i);
				Thread.sleep(this.tempo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("Chegou ao fim a Thread: " + this.nomeThread);	
	}

}

package br.com.meta.java.modulo1.capitulo24;

public interface IAnimalEstimacao {
  void brincar();
  void levarPassear();
}

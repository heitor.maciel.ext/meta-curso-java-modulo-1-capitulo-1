package br.com.meta.java.modulo1.capitulo24;

public class Cavalo extends Mamifero implements IAnimalDomesticado {

  @Override
  public void amamentar() {
    System.out.println("Amamentação do Cavalo");
  }

  @Override
  public void emitirSom() {
    System.out.println("Emitindo o som de um cavalo");
  }

  @Override
  public void alimentar() {
    System.out.println("Alimentando o cavalo");
  }

  @Override
  public void levarVeterinario() {
    System.out.println("Levando o cavalo no veterinario");
  }

  @Override
  public void chamarVeterinario() {
    System.out.println("Chamando o veterinário do cavalo");
  }

}

package br.com.meta.java.modulo1.capitulo24;

public class Cachorro extends Mamifero implements IAnimalDomesticado, IAnimalEstimacao {
  private String tamanho;
  private String raca;
  
  public String getRaca() {
    return raca;
  }
  public void setRaca(String raca) {
    this.raca = raca;
  }
  public String getTamanho() {
    return tamanho;
  }
  public void setTamanho(String tamanho) {
    this.tamanho = tamanho;
  }
  
  @Override
  public void amamentar() {
    System.out.println("Realiza a amamentação de um cachorro");
  }
  @Override
  public void emitirSom() {
    System.out.println("Emitir som de um cachorro - AU AU AU");
    
  }
  @Override
  public void brincar() {
    System.out.println("Brincando com o cachorro");
  }
  @Override
  public void levarPassear() {
    System.out.println("Levando o cachorro para passear");
    
  }
  @Override
  public void alimentar() {
    System.out.println("Alimentando o cachorro");
  }
  @Override
  public void levarVeterinario() {
    System.out.println("Levando cachorro para o veterinário");
  }
  @Override
  public void chamarVeterinario() {
    System.out.println("Chamando o veterinário do cachorro");
  }  
}

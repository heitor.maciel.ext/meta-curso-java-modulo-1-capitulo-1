package br.com.meta.java.modulo1.capitulo24;

public class Gato extends Mamifero implements IAnimalDomesticado, IAnimalEstimacao {
  private String raca;

  public String getRaca() {
    return raca;
  }

  public void setRaca(String raca) {
    this.raca = raca;
  }

  @Override
  public void amamentar() {
    System.out.println("Realiza a amamentação de um gato");
    
  }

  @Override
  public void emitirSom() {
    System.out.println("Emite o som de um gato - MIAU");
  }

  @Override
  public void brincar() {
    System.out.println("Brincando com o gato");
  }

  @Override
  public void levarPassear() {
    System.out.println("Levando o gato para passear");
  }

  @Override
  public void alimentar() {
    System.out.println("Alimentando o gato");
  }

  @Override
  public void levarVeterinario() {
    System.out.println("Levando o gato para o veterinário");
  }

  @Override
  public void chamarVeterinario() {
    System.out.println("Chamando o veterinário do gato");
    
  }

}

package br.com.meta.java.modulo1.capitulo24;

public class MainInterface {

  public static void main(String[] args) {
    Papagaio papagaio = new Papagaio();
    papagaio.emitirSom();
    papagaio.voar();
    
    Gato gato = new Gato();
    gato.emitirSom();
    gato.amamentar();
    
    Cachorro cachorro = new Cachorro();
    cachorro.emitirSom();
    gato.amamentar();
    
    //Animal animal = new Animal();
    //Mamifero mamifero = new Mamifero();
    //Ave ave = new Ave();
    
    System.out.println(IAnimalDomesticado.ANO_DOMESTICACAO);
  }
}

package br.com.meta.java.modulo1.capitulo24.exercicio;

public class Papagaio extends Ave {

  @Override
  public void voar() {
    System.out.println("Voar");
  }

  @Override
  public void emitirSom() {
    System.out.println("Emitir som");
  }
  /*
   * public void voar() { System.out.println("Voar"); }
   * 
   * public void emitirSom() { System.out.println("Emitir som"); }
   */
}

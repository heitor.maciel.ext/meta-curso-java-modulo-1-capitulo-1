package br.com.meta.java.modulo1.capitulo24.exercicio;

public interface IAnimalDomesticado {
  void alimentar();
  void levarVeterinario();
  void chamarVeterinario();
}

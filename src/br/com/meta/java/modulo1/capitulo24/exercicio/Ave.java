package br.com.meta.java.modulo1.capitulo24.exercicio;

public abstract class Ave extends Animal {
  public abstract void voar();
}

package br.com.meta.java.modulo1.capitulo24.exercicio;

public class Cachorro extends Mamifero implements IAnimalDomesticado, IAnimalEstimacao {
  String tamanho;
  String raca;
  
  public String getRaca() {
    return raca;
  }
  public void setRaca(String raca) {
    this.raca = raca;
  }
  public String getTamanho() {
    return tamanho;
  }
  public void setTamanho(String tamanho) {
    this.tamanho = tamanho;
  }
  @Override
  public void amamentar() {
    System.out.println("Amamentação do cachorro");
  }
  @Override
  public void emitirSom() {
    System.out.println("Au au");
  }
  @Override
  public void alimentar() {
    // TODO Auto-generated method stub
    
  }
  @Override
  public void levarVeterinario() {
    // TODO Auto-generated method stub
    
  }
  @Override
  public void chamarVeterinario() {
    // TODO Auto-generated method stub
    
  }
  @Override
  public void brincar() {
    // TODO Auto-generated method stub
    
  }
  @Override
  public void passear() {
    // TODO Auto-generated method stub
    
  }
  
  
}

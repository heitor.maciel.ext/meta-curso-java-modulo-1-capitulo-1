package br.com.meta.java.modulo1.capitulo24.exercicio;

public abstract class Animal {
  private String nome;

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }
  
  public abstract void emitirSom();
}

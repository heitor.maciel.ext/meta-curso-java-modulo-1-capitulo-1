package br.com.meta.java.modulo1.capitulo24.exercicio;

public class Cavalo extends Mamifero implements IAnimalDomesticado {
  @Override
  public void amamentar() {
    System.out.println("Amamentar cavalo");
  }

  @Override
  public void emitirSom() {
    System.out.println("Relinchando");
  }

  @Override
  public void alimentar() {
    System.out.println("Dar ração ao cavalo");
  }

  @Override
  public void levarVeterinario() {
    System.out.println("levar cavalo no veterinario");
  }

  @Override
  public void chamarVeterinario() {
    System.out.println("Chamar veterinario do cavalo");
  }
}

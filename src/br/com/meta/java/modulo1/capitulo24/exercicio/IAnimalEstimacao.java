package br.com.meta.java.modulo1.capitulo24.exercicio;

public interface IAnimalEstimacao {
  void brincar();
  void passear();
}

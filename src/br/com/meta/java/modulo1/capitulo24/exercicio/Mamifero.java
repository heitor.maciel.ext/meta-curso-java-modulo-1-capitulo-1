package br.com.meta.java.modulo1.capitulo24.exercicio;

public abstract class Mamifero extends Animal {
  public abstract void amamentar();
}

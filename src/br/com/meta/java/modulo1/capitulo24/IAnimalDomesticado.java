package br.com.meta.java.modulo1.capitulo24;

public interface IAnimalDomesticado {
  /**
   * Interface não tem atributos, porém ela tem constantes
   * Exemplo:
   */
  public static final Integer ANO_DOMESTICACAO = 2010;
  
  void alimentar();
  void levarVeterinario();
  void chamarVeterinario();
}

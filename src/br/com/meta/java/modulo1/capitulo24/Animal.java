package br.com.meta.java.modulo1.capitulo24;

public abstract class Animal {
  private String nome;
  
  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }
  
  /**
   * Métodos abstratos
   */
  public abstract void emitirSom();
}

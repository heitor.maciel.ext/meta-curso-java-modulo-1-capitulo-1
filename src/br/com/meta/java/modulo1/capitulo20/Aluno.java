package br.com.meta.java.modulo1.capitulo20;

import java.util.ArrayList;
import java.util.List;

public class Aluno extends Pessoa {
  private String curso;
  
  public Aluno() {
    super();  
  }
  
  public void verificarAcesso() {
    
  }
    
  public Aluno(String nome, String endereco, String telefone, String curso) {
    super(nome, endereco, telefone);
    this.curso = curso;
  }

  public String getCurso() {
    return curso;
  }
  public void setCurso(String curso) {
    this.curso = curso;
  }  
  public double calcularMedia() {
    return 0;
  }
  
  public boolean verificarAlunoAprovado() {
    return true;
  }
  
  public void setarCPFManualmente(String cpf) {
    super.setCpf(cpf);
    
    super.metodoEscrever();
    // ou
    this.metodoEscrever();
  }
  
  public List<Aluno> buscaAlunos() {
    return new ArrayList<Aluno>();
  }
  
  public List<Aluno> buscaAlunos(Integer IdCidade) {
    return new ArrayList<Aluno>();
  }
  
  public List<Aluno> buscaAlunos(Integer CodigoEstado, Integer CodigoCidade) {
    return new ArrayList<Aluno>();
  }

  @Override
  public void calculaIMC() {
    System.out.println("Calcula o IMC do ALUNO");
  }
  
}

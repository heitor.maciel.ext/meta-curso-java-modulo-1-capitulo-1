package br.com.meta.java.modulo1.capitulo20;

public class TesteMain {

  public static void main(String[] args) {
    // Sobre carga de métodos
    
    Aluno aluno1 = new Aluno();
    aluno1.buscaAlunos();
    // ou então é possível chamar o 
    // método com a mesma assinatura passando a região
    aluno1.buscaAlunos(10);
    
    /**
     * Exemplos de polimorfismo
     */
    Pessoa pessoa;
    
    Aluno aluno = new Aluno();
    Professor professor = new Professor();
    pessoa = aluno;
    
    pessoa.calculaIMC();
    //aluno.calculaIMC();
    pessoa = professor;
    pessoa.calculaIMC();
    
    // Descomentar o polimorfismo
    
    
    
  }

}

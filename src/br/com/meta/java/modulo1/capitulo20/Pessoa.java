package br.com.meta.java.modulo1.capitulo20;

public class Pessoa {
  private String nome;
  private String endereco;
  private String telefone;
  private String cpf;
  
  public Pessoa() {
    
  }

  public Pessoa(String nome, String endereco, String telefone) {
    super();
    this.nome = nome;
    this.endereco = endereco;
    this.telefone = telefone;
  }
  
  public String getNome() {
    return nome;
  }
  public void setNome(String nome) {
    this.nome = nome;
  }
  public String getEndereco() {
    return endereco;
  }
  public void setEndereco(String endereco) {
    this.endereco = endereco;
  }
  public String getCpf() {
    return cpf;
  }
  public void setCpf(String cpf) {
    this.cpf = cpf;
  }
  public String getTelefone() {
    return telefone;
  }
  public void setTelefone(String telefone) {
    this.telefone = telefone;
  }
  
  public void metodoEscrever() {
    System.out.println("Método Escrever");
  }
  
  public void calculaIMC() {
    System.out.println("Calcula o IMC da PESSOA");
  }
 }

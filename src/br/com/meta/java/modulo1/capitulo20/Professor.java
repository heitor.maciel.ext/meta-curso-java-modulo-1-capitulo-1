package br.com.meta.java.modulo1.capitulo20;

import java.math.BigDecimal;

public class Professor extends Pessoa {
  private BigDecimal salario;
  private String nomeCurso;
  
  public BigDecimal getSalario() {
    return salario;
  }
  public void setSalario(BigDecimal salario) {
    this.salario = salario;
  }
  public String getNomeCurso() {
    return nomeCurso;
  }
  public void setNomeCurso(String nomeCurso) {
    this.nomeCurso = nomeCurso;
  }
  
  public double calcularSalarioLiquido() {
    return 0;
  }
  
  @Override
  public void calculaIMC() {
    System.out.println("Calcular o IMC do PROFESSOR");
  }
  
}

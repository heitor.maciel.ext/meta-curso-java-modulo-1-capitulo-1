package br.com.meta.java.modulo1.capitulo10;

import java.util.Scanner;

public class ComandoBreak {

  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    
    System.out.println("Informe o inicio do for");
    int inicio = scan.nextInt();
    
    System.out.println("Informe o fim do for");
    int maximo = scan.nextInt();
    
    for (int i = inicio; i < maximo; i++) {
      if (i % 7 == 0) {
        System.out.println("O primeiro numero divisivel por 7 é: " + i);
        break;
      }
    }
  }

}

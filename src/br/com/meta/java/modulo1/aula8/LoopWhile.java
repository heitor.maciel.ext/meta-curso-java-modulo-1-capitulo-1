package br.com.meta.java.modulo1.aula8;

public class LoopWhile {

  public static void main(String[] args) {
    int i = 0;
    
    // Enquanto
    while (i < 10) {
      System.out.println("Valor da variável i: " + i);
      i++;
    }
  }

}

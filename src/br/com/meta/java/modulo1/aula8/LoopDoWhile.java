package br.com.meta.java.modulo1.aula8;

public class LoopDoWhile {

  public static void main(String[] args) {
    int i = 10;
    
    // Faça enquanto
    do {
      System.out.println("O valor de i é: " + i);
      i++;
    } while(i < 10);
  }

}

package br.com.meta.java.modulo1.capitulo36;

import br.com.meta.java.modulo1.capitulo20.Professor;

public class MainGarbageCollector {
	
	public static void getMemoryUsage() {
		final int MB = 1024 * 1024;
		
		Runtime runtime = Runtime.getRuntime();
		
		System.out.println((runtime.totalMemory() - runtime.freeMemory()) / MB);
	}
	
	public static void main(String[] args) {
		Professor[] professores = new Professor[30000000];
		Professor professor;
		
		for (int i = 0; i < professores.length; i++) {
			professor = new Professor();
			professor.setCpf("07503436999");
			professor.setNome("Heitor Maciel");
			professor.setNomeCurso("Curso de Java Básico");
			professor.setTelefone("41996142896");
			
			professores[i] = professor;
		}
		
		System.out.println("Professores instanciados no array");
		getMemoryUsage();
		professores = null;
		
		Runtime.getRuntime().runFinalization();
		Runtime.getRuntime().gc();
		
		System.out.println("Professores removidos da memória");
		getMemoryUsage();
	}

}

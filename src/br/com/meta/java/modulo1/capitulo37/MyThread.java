package br.com.meta.java.modulo1.capitulo37;

public class MyThread extends Thread {
	private String nomeThread;
  
  	public MyThread() {}

	public MyThread(String nomeThread) {
		super();
		this.nomeThread = nomeThread;
	}

	@Override
	public void run() {
		try {
			for (int i = 0; i < 10; i++) {
				System.out.println("Executando " + this.nomeThread + " contador: " + i);
				Thread.sleep(600);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println(this.nomeThread + " chegou ao fim");
	}
	
	public String getNomeThread() {
		return nomeThread;
	}

	public void setNomeThread(String nomeThread) {
		this.nomeThread = nomeThread;
	}
}

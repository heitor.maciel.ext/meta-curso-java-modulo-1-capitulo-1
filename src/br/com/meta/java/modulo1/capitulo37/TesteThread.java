package br.com.meta.java.modulo1.capitulo37;

public class TesteThread {

	public static void main(String[] args) {
		MyThread thread = new MyThread("Thread #1");
		thread.start();
		
		MyThread thread2 = new MyThread("Thread #2");
		thread2.start();
	}

}

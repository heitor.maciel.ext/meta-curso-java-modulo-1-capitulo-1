package br.com.meta.java.modulo1.capitulo39;

public class TesteRunnable2 {

	public static void main(String[] args) {
		ThreadRunnable runnable = new ThreadRunnable("Thread #1", 600);
		Thread thread = new Thread(runnable);
		
		ThreadRunnable runnable2 = new ThreadRunnable("Thread #2", 600);
		Thread thread2 = new Thread(runnable2);
		
		
		ThreadRunnable runnable3 = new ThreadRunnable("Thread #3", 600);
		Thread thread3 = new Thread(runnable3);
		
		thread.start();
		thread2.start();
		thread3.start();
		
		while (thread.isAlive() || thread2.isAlive() || thread3.isAlive()) {}
		
		System.out.println("Programa Finalizado");
	}

}

package br.com.meta.java.modulo1.capitulo17;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Empresa {
  String nome;
  String cnpj;
  Integer quantidadeFuncionario;
  SetorAtuacao setorAtuacao;
  List<Proprietario> proprietarios = new ArrayList<Proprietario>();
  List<Funcionario> funcionarios = new ArrayList<Funcionario>();
  List<Servico> servicosPrestados = new ArrayList<Servico>();
  Contador contador;
  //Empresa empresaContabilidade;
  
  public Empresa() {
    // TODO Auto-generated constructor stub
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getCnpj() {
    return cnpj;
  }

  public void setCnpj(String cnpj) {
    this.cnpj = cnpj;
  }

  public Integer getQuantidadeFuncionario() {
    return quantidadeFuncionario;
  }

  public void setQuantidadeFuncionario(Integer quantidadeFuncionario) {
    this.quantidadeFuncionario = quantidadeFuncionario;
  }

  public List<Proprietario> getProprietarios() {
    return proprietarios;
  }

  public void setProprietarios(List<Proprietario> proprietarios) {
    this.proprietarios = proprietarios;
  }

  public List<Funcionario> getFuncionarios() {
    return funcionarios;
  }

  public void setFuncionarios(List<Funcionario> funcionarios) {
    this.funcionarios = funcionarios;
  }

  public Contador getContador() {
    return contador;
  }

  public void setContador(Contador contador) {
    this.contador = contador;
  }

  public List<Servico> getServicosPrestados() {
    return servicosPrestados;
  }

  public void setServicosPrestados(List<Servico> servicosPrestados) {
    this.servicosPrestados = servicosPrestados;
  }
  
  public static Proprietario getProprietario() {
    Scanner scan = new Scanner(System.in);
    Proprietario _rProprietario = new Proprietario();
    System.out.println("Informe o nome do proprietario: ");
    _rProprietario.setNome(scan.nextLine());
    
    System.out.println("Informe a idade do proprietario: ");
    _rProprietario.setIdade(scan.nextInt());
    
    return _rProprietario;
  }
  
  public void listaProprietarios() {
    StringBuilder _rProprietarios = new StringBuilder();
    
    for (Proprietario proprietario : this.getProprietarios()) {
      _rProprietarios.append(proprietario.toString());
      System.out.println("----------------------------------------");
    }
    
    System.out.println(_rProprietarios);
  }
  
  public static Funcionario getFuncionario() {
    Scanner scan = new Scanner(System.in);
    Funcionario _rFuncionario = new Funcionario();
    System.out.println("Informe o nome do funcionario: ");
    _rFuncionario.setNome(scan.nextLine());
    
    System.out.println("Informe a idade do funcionario: ");
    _rFuncionario.setIdade(scan.nextInt());
    
    System.out.println("Informe o salario do funcionario: ");
    _rFuncionario.setSalario(scan.nextBigDecimal());
    
    return _rFuncionario;
  }

  public void listaFuncionarios() {
    StringBuilder _rFuncionarios = new StringBuilder();
    
    for (Funcionario funcionario : this.getFuncionarios()) {
      _rFuncionarios.append(funcionario.toString());
      System.out.println("----------------------------------------");
    }
    
    System.out.println(_rFuncionarios);
  }
  
  public static Servico getServico() {
    Scanner scan = new Scanner(System.in);
    
    Servico _rServico = new Servico();
    System.out.println("Informe o nome do serviço: ");
    _rServico.setNome(scan.nextLine());
    
    System.out.println("Informe a idade da descricao: ");
    _rServico.setDescricao(scan.nextLine());
    
    System.out.println("Informe o valor do serviço: ");
    _rServico.setValor(scan.nextBigDecimal());
    
    return _rServico;
  }
  
  public void listaServicos() {
    StringBuilder _rServicos = new StringBuilder();
    
    for (Servico servico : this.getServicosPrestados()) {
      _rServicos.append(servico.toString());
      System.out.println("----------------------------------------");
    }
    
    System.out.println(_rServicos);
  }
}

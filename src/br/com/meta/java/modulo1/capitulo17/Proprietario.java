package br.com.meta.java.modulo1.capitulo17;

public class Proprietario extends Pessoa {
  Integer quantidadeEmpresas = 1;
  Formacao formacao;
  
  public Proprietario() {
    super();
  }
  
  public Integer getQuantidadeEmpresas() {
    return quantidadeEmpresas;
  }
  public void setQuantidadeEmpresas(Integer quantidadeEmpresas) {
    this.quantidadeEmpresas = quantidadeEmpresas;
  }

  @Override
  public String toString() {
    return "Proprietario [quantidadeEmpresas=" + quantidadeEmpresas + ", formacao=" + formacao + ", nome=" + nome
        + ", idade=" + idade + ", cidade=" + cidade + "]";
  }
}

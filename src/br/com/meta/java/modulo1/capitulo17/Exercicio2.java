package br.com.meta.java.modulo1.capitulo17;

import java.util.Scanner;

public class Exercicio2 {

  public static void main(String[] args) {
    @SuppressWarnings("resource")
    Scanner _rScan = new Scanner(System.in);
    Boolean _rContinua = true;
    String menu = getMenu();
    Empresa _rEmpresa = new Empresa();
    
    System.out.println("Ola, seja bem vindo ao sistema de cadastro de empresas:");
    System.out.println("Informe por favor o nome da sua empresa:");
    _rEmpresa.setNome(_rScan.nextLine());
    
    System.out.println("Informe por favor o cnpj:");
    _rEmpresa.setCnpj(_rScan.nextLine());
    
    while (_rContinua) {
      System.out.println(menu);
      Integer opcao = _rScan.nextInt();
      
      switch (opcao) {
      case 1: // CADASTRAR_PROPRIETARIO
        Proprietario proprietario = new Proprietario();
        proprietario = Empresa.getProprietario();
        
        _rEmpresa.getProprietarios().add(proprietario);
        break;
      case 2: // CADASTRAR_FUNCIONARIO
        Funcionario funcionario = new Funcionario();
        funcionario = Empresa.getFuncionario();
        
        _rEmpresa.getFuncionarios().add(funcionario);        
        break;
      case 3: // CADASTRAR_SERVICO
        Servico servico = new Servico();
        servico = Empresa.getServico();
        
        _rEmpresa.getServicosPrestados().add(servico);
        break;
      case 4: // LISTAR_PROPRIETARIO
        _rEmpresa.listaProprietarios();
        break;
      case 5: // LISTAR_FUNCIONARIO
        _rEmpresa.listaFuncionarios();
        break;
      case 6: // LISTAR_SERVICO
        _rEmpresa.listaServicos();
        break;
      case 7: // SAIR
        _rContinua = false;
        break;
      default:
        System.out.println("A opção escolhida é inválida, por favor selecione uma opção válida" + System.lineSeparator());
        System.out.println("");
        break;
      }
    }
  }
  
  public static String getMenu() {
    MenuAplicacao[] menuAplicacao = MenuAplicacao.values();
    StringBuilder _rMenu = new StringBuilder();
    
    _rMenu.append("Opções do menu: " + System.lineSeparator());
    
    for (MenuAplicacao menu : menuAplicacao)
      _rMenu.append(menu.getCodigo() + " - " + menu.getOpcaoMenu() + System.lineSeparator());
    
    return _rMenu.toString();
  }
}

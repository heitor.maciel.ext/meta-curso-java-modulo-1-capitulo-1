package br.com.meta.java.modulo1.capitulo17;

public enum SetorAtuacao {
  FINANCEIRO(10) {
    @Override
    public String getDescricaoSetorAtuacao() {
      // TODO Auto-generated method stub
      return "O Setor de atuação financeiro consiste em realizar empréstimos bancários...";
    }
  },
  TECNOLOGIA(20) {
    @Override
    public String getDescricaoSetorAtuacao() {
      // TODO Auto-generated method stub
      return "O Setor de atuação tecnologia consiste em realizar desenvolvimento de softwares...";
    }
  },
  IMOBILIARIO(30) {
    @Override
    public String getDescricaoSetorAtuacao() {
      // TODO Auto-generated method stub
      return "O Setor de atuação tecnologia consiste em realizar a venda de casas...";
    }
  },
  INDUSTRIA(40) {
    @Override
    public String getDescricaoSetorAtuacao() {
      // TODO Auto-generated method stub
      return "Não sei dizer";
    }
  };
  
  private int codigo;
  
  SetorAtuacao(int codigo) {
    this.codigo = codigo;
  }
  
  public int getCodigoSetor() {
    return this.codigo;
  }
  
  public abstract String getDescricaoSetorAtuacao();
}

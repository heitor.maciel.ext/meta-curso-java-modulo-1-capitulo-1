package br.com.meta.java.modulo1.capitulo17;

import java.math.BigDecimal;

public class Servico {
  String nome;
  String descricao;
  BigDecimal valor;
  
  public Servico() {

  }
  
  public String getNome() {
    return nome;
  }
  public void setNome(String nome) {
    this.nome = nome;
  }
  public String getDescricao() {
    return descricao;
  }
  public void setDescricao(String descricao) {
    this.descricao = descricao;
  }

  public BigDecimal getValor() {
    return valor;
  }

  public void setValor(BigDecimal valor) {
    this.valor = valor;
  }

  @Override
  public String toString() {
    return "Servico [nome=" + nome + ", descricao=" + descricao + ", valor=" + valor + ", getNome()=" + getNome()
        + ", getDescricao()=" + getDescricao() + ", getValor()=" + getValor() + "]";
  }
}

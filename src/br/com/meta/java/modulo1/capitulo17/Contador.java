package br.com.meta.java.modulo1.capitulo17;

import java.util.ArrayList;
import java.util.List;

public class Contador extends Pessoa {
  List<Servico> servicos = new ArrayList<Servico>();
  
  public Contador() {
    super();
  }
  
  public List<Servico> getServicos() {
    return servicos;
  }
  public void setServicos(List<Servico> servicos) {
    this.servicos = servicos;
  }
  
}

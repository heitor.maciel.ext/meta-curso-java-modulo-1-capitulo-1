package br.com.meta.java.modulo1.capitulo17;

public enum SetorEmpresa {
  RH,
  TECNOLOGIA,
  ADMINISTRACAO,
  SECRETARIA,
  MARKETING,
  MARKETING_DIGITAL,
  FINANCEIRO
}

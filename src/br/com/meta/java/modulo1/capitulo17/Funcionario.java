package br.com.meta.java.modulo1.capitulo17;

import java.math.BigDecimal;

public class Funcionario extends Pessoa {
  BigDecimal salario;
  SetorEmpresa setorEmpresa;
  
  public Funcionario() {
    super();
  }
  
  public BigDecimal getSalario() {
    return salario;
  }
  public void setSalario(BigDecimal salario) {
    this.salario = salario;
  }

  @Override
  public String toString() {
    return "Funcionario [salario=" + salario + ", setorEmpresa=" + setorEmpresa + ", nome=" + nome + ", idade=" + idade
        + ", cidade=" + cidade + "]";
  }
  
}

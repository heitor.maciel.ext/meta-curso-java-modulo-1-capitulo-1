package br.com.meta.java.modulo1.capitulo17;

public enum Formacao {
  SISTEMAS_INFORMACAO,
  CIENCIA_DA_COMPUTACAO,
  CONTABILIDADE,
  ODONTOLOGIA,
  ADMINISTRACAO
}

package br.com.meta.java.modulo1.capitulo17;

public enum MenuAplicacao {
  CADASTRAR_PROPRIETARIO(1) {
    @Override
    public String getOpcaoMenu() {
      return "Cadastrar proprietario";
    }
  },
  CADASTRAR_FUNCIONARIO(2) {
    @Override
    public String getOpcaoMenu() {
      return "Cadastrar funcionário";
    }
  },
  CADASTRAR_SERVICO(3) {
    @Override
    public String getOpcaoMenu() {
      return "Cadastrar serviço";
    }
  },
  LISTAR_PROPRIETARIO(4) {
    @Override
    public String getOpcaoMenu() {
      return "Listar proprietarios";
    }
  },
  LISTAR_FUNCIONARIO(5) {
    @Override
    public String getOpcaoMenu() {
      return "Listar funcionario";
    }
  },
  LISTAR_SERVICO(6) {
    @Override
    public String getOpcaoMenu() {
      return "Listar serviços prestados";
    }
  };
  
  private int codigo;
  
  MenuAplicacao(int codigo) {
    this.codigo = codigo;
  }
  
  public int getCodigo() {
    return this.codigo;
  }
  
  public abstract String getOpcaoMenu();
}

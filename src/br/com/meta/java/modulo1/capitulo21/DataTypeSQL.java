package br.com.meta.java.modulo1.capitulo21;

public enum DataTypeSQL {
  NUMERIC,
  VARCHAR,
  CHAR
}

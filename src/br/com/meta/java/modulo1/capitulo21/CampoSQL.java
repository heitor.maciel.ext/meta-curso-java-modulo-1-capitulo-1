package br.com.meta.java.modulo1.capitulo21;

public class CampoSQL {
  String nomeCampo;
  Object valorCampo;
  DataTypeSQL datatype;
  
  public CampoSQL() {

  }

  public CampoSQL(String nomeCampo, Object valorCampo, DataTypeSQL datatype) {
    super();
    this.nomeCampo = nomeCampo;
    this.datatype = datatype;
    this.valorCampo = valorCampo;
  }

  public String getNomeCampo() {
    return nomeCampo;
  }

  public void setNomeCampo(String nomeCampo) {
    this.nomeCampo = nomeCampo;
  }

  public Object getValorCampo() {
    return valorCampo;
  }

  public void setValorCampo(Object valorCampo) {
    this.valorCampo = valorCampo;
  }
  
  
}

package br.com.meta.java.modulo1.capitulo21;

import java.util.ArrayList;
import java.util.List;

public class TesteInterface {

  public static void main(String[] args) {
    HelperSQL helper = new HelperSQL(new InterfaceSQL() {
      @Override
      public String getNomeTabela() {
        // TODO Auto-generated method stub
        return "PAGAMENTOS";
      }
      
      @Override
      public List<CampoSQL> getCamposSQL() {
        List<CampoSQL> campos = new ArrayList<CampoSQL>();
        campos.add(new CampoSQL("VL_PAGAME", 200, DataTypeSQL.NUMERIC));
        campos.add(new CampoSQL("VL_JUROS", 2200, DataTypeSQL.NUMERIC));
        campos.add(new CampoSQL("TX_JUROS", 5, DataTypeSQL.NUMERIC));
        campos.add(new CampoSQL("DS_OBSERVACAO", "Essa conta foi paga manualmente", DataTypeSQL.NUMERIC));
        return campos;
      }
    });
    
    helper.insert();
    System.out.println("");
    helper.select();
    
    
    HelperSQL helpersql = new HelperSQL(new InterfaceSQL() {
      @Override
      public String getNomeTabela() {
        return "agendamentos";
      }
      
      @Override
      public List<CampoSQL> getCamposSQL() {
        List<CampoSQL> campos = new ArrayList<CampoSQL>();
        campos.add(new CampoSQL("dt_agendamento", "01/10/2019", DataTypeSQL.VARCHAR));
        campos.add(new CampoSQL("vlr_agendamento", 500, DataTypeSQL.NUMERIC));
        return campos;
      }
    });
    
    helpersql.insert();
    System.out.println("");
    helpersql.select();
  }

}

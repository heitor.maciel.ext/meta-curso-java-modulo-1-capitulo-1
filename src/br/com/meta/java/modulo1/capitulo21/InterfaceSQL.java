package br.com.meta.java.modulo1.capitulo21;

import java.util.List;

public interface InterfaceSQL {
  String getNomeTabela();
  List<CampoSQL> getCamposSQL();
}

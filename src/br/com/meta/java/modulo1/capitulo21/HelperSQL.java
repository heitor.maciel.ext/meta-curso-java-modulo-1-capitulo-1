package br.com.meta.java.modulo1.capitulo21;

import java.util.List;

public class HelperSQL {
  InterfaceSQL interfaceSQL;
  
  public HelperSQL() {

  }

  public HelperSQL(InterfaceSQL interfaceSQL) {
    super();
    this.interfaceSQL = interfaceSQL;
  }
  
  public void insert() {
    List<CampoSQL> camposSQL = this.interfaceSQL.getCamposSQL();
    StringBuilder builder = new StringBuilder();
    String campos = new String();
    String valores = new String();
    
    
    builder.append("INSERT INTO " + this.interfaceSQL.getNomeTabela() + " (");
    
    for (CampoSQL campoSQL : camposSQL)
      campos = campos + campoSQL.getNomeCampo() + ",";
    
    campos = campos.substring(0, campos.length()-1);
    
    builder.append(campos + ") VALUES (");
    
    for (CampoSQL campoSQL : camposSQL)
      valores = valores + "'" + campoSQL.getValorCampo() + "',";
    
    valores = valores.substring(0, valores.length()-1);
    
    builder.append(valores + ");");
    
    System.out.println(builder.toString());
  }
  
  public void select() {
    List<CampoSQL> camposSQL = this.interfaceSQL.getCamposSQL();
    StringBuilder builder = new StringBuilder();
    String campos = new String();
    
    builder.append("SELECT ");
    
    for (CampoSQL campoSQL : camposSQL)
      campos = campos + campoSQL.getNomeCampo() + ",";
    
    campos = campos.substring(0, campos.length()-1);
    
    builder.append(campos);
    builder.append(" FROM " + this.interfaceSQL.getNomeTabela() + ";");
    
    System.out.println(builder.toString());
  }
}

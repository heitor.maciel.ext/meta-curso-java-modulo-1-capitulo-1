package br.com.meta.java.modulo1.capitulo34;

public class TesteInterfaceAnonima {

	public static void main(String[] args) {
		InterfaceAnonima anonima = new InterfaceAnonima() {
			@Override
			public void imprimirTexto() {
				System.out.println("Imprimindo interface anonima");
			}
		};
		
		anonima.imprimirTexto();
		
		anonima = new InterfaceAnonima() {
			@Override
			public void imprimirTexto() {
				System.out.println("Heitor");
			}
		};
		
		anonima.imprimirTexto();
	}

}

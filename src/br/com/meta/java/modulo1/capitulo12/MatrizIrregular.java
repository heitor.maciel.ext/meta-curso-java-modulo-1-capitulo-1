package br.com.meta.java.modulo1.capitulo12;

public class MatrizIrregular {

  public static void main(String[] args) {
    // Posição 1 - Bimestre 
    //     0 - 1 Bimestre
    //     1 - 2 Bimestre
    //     2 - 3 Bimestre
    //     3 - 4 Bimestre
    
    // Posição 2 - Materia
    //     0 - Portugues
    //     1 - Matemática
    //     ...........
    //     Várias máterias

    // Posição 3 - Aluno
    //     0 - Heitor
    //     1 - Josué
    //     ...........
    //     Vários alunos
    
    double[][][] notasAluno = null;
    
    // Nota Heitor - Portugues 1 Bimestre
    notasAluno[0][0][0] = 8;
    // Nota Heitor - Portugues 2 Bimestre
    notasAluno[1][0][0] = 7;
    // Nota Heitor - Portugues 3 Bimestre
    notasAluno[2][0][0] = 3;
    // Nota Heitor - Portugues 4 Bimestre
    notasAluno[3][0][0] = 5;
    
    // Nota Heitor - Matematica 1 Bimestre
    notasAluno[0][1][0] = 8;
    // Nota Heitor - Matematica 2 Bimestre
    notasAluno[1][1][0] = 1;
    // Nota Heitor - Matematica 3 Bimestre
    notasAluno[2][1][0] = 4;
    // Nota Heitor - Matematica 4 Bimestre
    notasAluno[3][1][0] = 8;
    
    /**************************************************************/
    
    // Nota Josué - Portugues 1 Bimestre
    notasAluno[0][0][1] = 4;
    // Nota Josué - Portugues 2 Bimestre
    notasAluno[1][0][1] = 8;
    // Nota Josué - Portugues 3 Bimestre
    notasAluno[2][0][1] = 10;
    // Nota Josué - Portugues 4 Bimestre
    notasAluno[3][0][1] = 7;
    
    // Nota Josué - Matematica 1 Bimestre
    notasAluno[0][1][1] = 4;
    // Nota Josué - Matematica 2 Bimestre
    notasAluno[1][1][1] = 6;
    // Nota Josué - Matematica 3 Bimestre
    notasAluno[2][1][1] = 9;
    // Nota Josué - Matematica 4 Bimestre
    notasAluno[3][1][1] = 5;
    
    for (int i = 0; i < notasAluno.length; i++) {    
      for (int j = 0; j < notasAluno[i].length; j++) {
        for (int k = 0; k < notasAluno[i][j].length; k++) {
          System.out.print("Nota - ");
          
          switch (i) {
          case 0:
            System.out.print("1 Bimestre - ");
            break;
          case 1:
            System.out.print("2 Bimestre - ");
            break;
          case 2:
            System.out.print("3 Bimestre - ");
            break;
          case 3:
            System.out.print("4 Bimestre - ");
            break;
          default:
            break;
          }
          
          switch (j) {
          case 0:
            System.out.print("Aluno Heitor - ");
            break;
          case 1:
            System.out.print("Aluno Josué - ");
            break;
          default:
            break;
          }
          
          switch (k) {
          case 0:
            System.out.print("Português - ");
            break;
          case 1:
            System.out.print("Matemática - ");
            break;
          default:
            break;
          }
          
          System.out.println("Nota - " + notasAluno[i][j][k] + ";");
        }
      }
      
      System.out.println("");
    }
  }
}

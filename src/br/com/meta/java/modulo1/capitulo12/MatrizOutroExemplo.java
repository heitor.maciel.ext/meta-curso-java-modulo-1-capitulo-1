package br.com.meta.java.modulo1.capitulo12;

public class MatrizOutroExemplo {

  public static void main(String[] args) {
    double[][] notasAluno = {
      {10, 9, 6, 9}   , // 1 Aluno
      {2, 5, 4, 7}    , // 2 Aluno
      {8.5, 9, 3, 3.2}, // 3 Aluno
      {10, 10, 10, 10}, // 4 Aluno
      {9, 6.3, 2, 4}    // 5 Aluno
    };

    for (int i = 0; i < notasAluno.length; i++) {
      System.out.print("As notas do aluno " + (i + 1) + " são: ");
      
      for (int j = 0; j < notasAluno[i].length; j++) {
        System.out.print(notasAluno[i][j] + " / ");
      }
      
      System.out.println("");
      //System.out.println("As notas do aluno " + (i + 1) + " são: " + notasAluno[i]);
    }
  }

}

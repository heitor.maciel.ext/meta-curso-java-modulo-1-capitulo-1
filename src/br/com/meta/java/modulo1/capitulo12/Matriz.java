package br.com.meta.java.modulo1.capitulo12;

public class Matriz {

  public static void main(String[] args) {
    // Básico
    
    // Conhecida por muitos como: Array de Arrays
    
    // Problema: Guardar notas dos bimestres de 5 alunos
    
    // Solução 1: Essa seria solução? Seria a ideal?
    double[] aluno1 = new double[4];
    double[] aluno2 = new double[4];
    double[] aluno3 = new double[4];
    double[] aluno4 = new double[4];
    double[] aluno5 = new double[4];
    
    // Solução 2: Solução ideal
    double[][] notasAluno = new double[5][4];
    
    // Primeiro aluno
    notasAluno[0][0] = 7.0; // 1 Bimestre
    notasAluno[0][1] = 9.0; // 2 Bimestre
    notasAluno[0][2] = 4.8; // 3 Bimestre
    notasAluno[0][3] = 6.5; // 4 Bimestre
    
    // Segundo aluno
    notasAluno[1][0] = 9.0; // 1 Bimestre
    notasAluno[1][1] = 10; // 2 Bimestre
    notasAluno[1][2] = 8.0; // 3 Bimestre
    notasAluno[1][3] = 8.5; // 4 Bimestre
    
    // Terceiro aluno
    notasAluno[2][0] = 9.0; // 1 Bimestre
    notasAluno[2][1] = 10;  // 2 Bimestre
    notasAluno[2][2] = 8.0; // 3 Bimestre
    notasAluno[2][3] = 8.5; // 4 Bimestre
    
    // Quarto aluno
    notasAluno[3][0] = 1.0; // 1 Bimestre
    notasAluno[3][1] = 2.5;  // 2 Bimestre
    notasAluno[3][2] = 3.0; // 3 Bimestre
    notasAluno[3][3] = 9.5; // 4 Bimestre
    
    // Quarto aluno
    notasAluno[4][0] = 10.0; // 1 Bimestre
    notasAluno[4][1] = 10.0; // 2 Bimestre
    notasAluno[4][2] = 10.0; // 3 Bimestre
    notasAluno[4][3] = 10.0; // 4 Bimestre
    
    
    /*
     * for (int i = 0; i < notasAluno.length; i++) {
     * System.out.println("As notas do aluno " + (i + 1) + " são: " +
     * notasAluno[i]); }
     */
    
    for (int i = 0; i < notasAluno.length; i++) {
      System.out.print("As notas do aluno " + (i + 1) + " são: ");
      
      for (int j = 0; j < notasAluno[i].length; j++) {
        System.out.print(notasAluno[i][j] + " / ");
      }
      
      System.out.println("");
      //System.out.println("As notas do aluno " + (i + 1) + " são: " + notasAluno[i]);
    }
  }

}

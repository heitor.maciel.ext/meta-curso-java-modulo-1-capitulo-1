package br.com.meta.java.modulo1.capitulo26;

public class Aluno {
  private String curso;
  private double[] notas;
  
  public Aluno() {
    super();  
  }

  public String getCurso() {
    return curso;
  }

  public void setCurso(String curso) {
    this.curso = curso;
  }

  public double[] getNotas() {
    return notas;
  }

  public void setNotas(double[] notas) {
    this.notas = notas;
  }
    
}

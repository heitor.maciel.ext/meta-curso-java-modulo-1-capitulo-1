package br.com.meta.java.modulo1.capitulo30;

import java.sql.Connection;
import java.sql.DriverManager;

public class MainConexao {

	public static void main(String[] args) {
		try {
			Connection conn = DriverManager.getConnection(
			  "jdbc:postgresql://localhost:5432/spring-boot", "postgres", "maciel1992"
			);
			
            if (conn != null) {
                System.out.println("Conectou no banco de dados");
            } else {
                System.out.println("Falou ao conectar no banco de dados");
            }
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}

}

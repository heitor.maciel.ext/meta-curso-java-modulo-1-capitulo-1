package br.com.meta.java.modulo1.capitulo30;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class MainExemploSQL {

	public static void main(String[] args) {
		try {
			Connection conn = DriverManager.getConnection(
			  "jdbc:postgresql://localhost:5432/spring-boot", "postgres", "maciel1992"
			);
			
            if (conn != null) {
            	Statement statement = conn.createStatement();
                ResultSet resultSet = statement.executeQuery("select * from \"projeto-aula\".categoria");
                
                while (resultSet.next()) {
                	String valor = String.format("Id: %s - Nome: %s", resultSet.getInt("id"), resultSet.getString("nome"));
                	System.out.println(valor);
                }
            } else {
                System.out.println("Falou ao conectar no banco de dados");
            }
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}

}

package br.com.meta.java.modulo1.capitulo100.enumeradores;

public enum StatusCarro {
  LIGADO,
  DESLIGADO
}

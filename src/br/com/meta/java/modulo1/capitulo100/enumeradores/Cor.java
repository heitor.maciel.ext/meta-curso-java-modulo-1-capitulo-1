package br.com.meta.java.modulo1.capitulo100.enumeradores;

public enum Cor {
  VERMELHO,
  BRANCO,
  PRETO,
  AZUL,
  CINZA,
  AMARELO,
  ROSA
}

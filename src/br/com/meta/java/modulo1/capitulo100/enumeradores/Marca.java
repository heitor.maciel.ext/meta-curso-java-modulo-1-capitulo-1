package br.com.meta.java.modulo1.capitulo100.enumeradores;

public enum Marca {
  FORD(10), 
  CHEVROLET(20),
  VOLKSWAGEN(30),
  FIAT(40), 
  HYUNDAI(50);
  
  int codigo;
  
  Marca(int Codigo) {
    this.codigo = Codigo;
  }
  
  public int getValor() {
    return this.codigo;
  }
}

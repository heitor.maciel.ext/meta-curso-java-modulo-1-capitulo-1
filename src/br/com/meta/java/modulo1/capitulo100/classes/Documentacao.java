package br.com.meta.java.modulo1.capitulo100.classes;

public class Documentacao {
  String renavam;
  String ipva;
  String proprietario;
  String cpfProprietario;
  
  public Documentacao() {
    // TODO Auto-generated constructor stub
  }
  
  public String getRenavam() {
    return renavam;
  }
  public void setRenavam(String renavam) {
    this.renavam = renavam;
  }
  public String getIpva() {
    return ipva;
  }
  public void setIpva(String ipva) {
    this.ipva = ipva;
  }
  public String getProprietario() {
    return proprietario;
  }
  public void setProprietario(String proprietario) {
    this.proprietario = proprietario;
  }
  public String getCpfProprietario() {
    return cpfProprietario;
  }
  public void setCpfProprietario(String cpfProprietario) {
    this.cpfProprietario = cpfProprietario;
  }
  
}

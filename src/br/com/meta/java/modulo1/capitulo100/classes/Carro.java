package br.com.meta.java.modulo1.capitulo100.classes;

import br.com.meta.java.modulo1.capitulo100.enumeradores.Cor;
import br.com.meta.java.modulo1.capitulo100.enumeradores.Marca;
import br.com.meta.java.modulo1.capitulo100.enumeradores.StatusCarro;

public class Carro {
  Cor cor;
  Marca marca;
  String nome;
  Documentacao documentacao = new Documentacao();
  
  StatusCarro statusCarro = StatusCarro.DESLIGADO;
  
  double velocidadeAtual = 0;
  double velocidadeMaxima = 200;
  
  /**
   * @author heitormaciel
   * Exemplo de constructor vazio
   */
  public Carro() {

  }
  
  public Carro(Cor cor, Marca marca, String nome, StatusCarro statusCarro) {
    this.cor = cor;
    this.marca = marca;
    this.nome = nome;
    
    if (this.statusCarro != null)
      this.statusCarro = statusCarro;
  }
  
  public void ligar() {
    this.statusCarro = StatusCarro.LIGADO;
  }
  
  /**
   * @author heitormaciel
   * 
   * Se retornar true o carro esta ligado
   * Se retornar false o carro esta desligado
   */
  public boolean verificaStatusCarro() {
    if (this.statusCarro == StatusCarro.LIGADO)
      return true;
    else
      return false;
  }
  
  public void acelerar(double velocidade) {
    this.velocidadeAtual = this.velocidadeAtual + velocidade; 
  }
  
  public boolean validaUltrapassouLimite() {
    if (this.velocidadeAtual > this.velocidadeMaxima)
      return true;
    else
      return false;
  }

  public Cor getCor() {
    return cor;
  }

  public void setCor(Cor cor) {
    this.cor = cor;
  }

  public Marca getMarca() {
    return marca;
  }

  public void setMarca(Marca marca) {
    this.marca = marca;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public StatusCarro getStatusCarro() {
    return statusCarro;
  }

  public void setStatusCarro(StatusCarro statusCarro) {
    this.statusCarro = statusCarro;
  }

  public double getVelocidadeAtual() {
    return velocidadeAtual;
  }

  public void setVelocidadeAtual(double velocidadeAtual) {
    this.velocidadeAtual = velocidadeAtual;
  }

  public double getVelocidadeMaxima() {
    return velocidadeMaxima;
  }

  public void setVelocidadeMaxima(double velocidadeMaxima) {
    this.velocidadeMaxima = velocidadeMaxima;
  }

  public Documentacao getDocumentacao() {
    return documentacao;
  }

  public void setDocumentacao(Documentacao documentacao) {
    this.documentacao = documentacao;
  }
}

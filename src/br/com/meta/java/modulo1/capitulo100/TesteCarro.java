package br.com.meta.java.modulo1.capitulo100;

import br.com.meta.java.modulo1.capitulo100.classes.Carro;
import br.com.meta.java.modulo1.capitulo100.classes.Documentacao;
import br.com.meta.java.modulo1.capitulo100.enumeradores.Cor;
import br.com.meta.java.modulo1.capitulo100.enumeradores.Marca;
import br.com.meta.java.modulo1.capitulo100.enumeradores.StatusCarro;

public class TesteCarro {

  public static void main(String[] args) {
    // TODO Auto-generated method stub  
    //Carro car = new Carro(Cor.AZUL, Marca.FORD, "Ford Focus", StatusCarro.LIGADO);
    //System.out.println(car.getCor());
    //System.out.println(car.verificaStatusCarro());
    
    /*
     * Carro car = new Carro();
     * 
     * Documentacao doc = new Documentacao(); doc.setRenavam("asdjoiadsjdsaio");;
     * 
     * car.setMarca(Marca.FORD); car.setVelocidadeMaxima(600);
     */
    
    Marca marca = Marca.FIAT;
    
    System.out.println("O nome da marca é: " + marca.toString() + " | O código da marca é: " + marca.getValor());
  }

}

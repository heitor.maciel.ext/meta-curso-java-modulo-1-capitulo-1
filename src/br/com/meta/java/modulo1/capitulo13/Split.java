package br.com.meta.java.modulo1.capitulo13;

public class Split {

  public static void main(String[] args) {
    String valor = "Heitor - Maciel - Medeiros";
    String[] valorExemplo = valor.split("-");
     
    for(String palavra : valorExemplo){
       System.out.println(palavra.trim());
    }
  }

}

package br.com.meta.java.modulo1.capitulo13;

public class GetBytes {

  public static void main(String[] args) {
    String valor1 = "Essa é uma aula de Java para a Sascar";
    
    for (byte caracterbyte : valor1.getBytes()) {
      System.out.println("Caracter: " + caracterbyte);
    }
  }

}

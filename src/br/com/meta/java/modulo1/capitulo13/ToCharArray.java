package br.com.meta.java.modulo1.capitulo13;

public class ToCharArray {

  public static void main(String[] args) {
    String valor1 = "Essa é uma aula de Java para a Sascar";
    
    for (char caracter : valor1.toCharArray()) {
      System.out.println("Caracter: " + caracter);
    }
  }

}

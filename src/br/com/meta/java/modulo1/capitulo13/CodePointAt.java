package br.com.meta.java.modulo1.capitulo13;

public class CodePointAt {

  public static void main(String[] args) {
    // Retorna o caractere de uma determinada 
    
    String valor1 = "Essa é uma string referente a aula de java";
    
    System.out.println(valor1.codePointAt(5));
  }

}

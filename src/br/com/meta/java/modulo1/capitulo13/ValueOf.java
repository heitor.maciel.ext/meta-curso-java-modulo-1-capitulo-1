package br.com.meta.java.modulo1.capitulo13;

public class ValueOf {

  public static void main(String[] args) {
    boolean variavelBoolean = true;       
    System.out.println(String.valueOf(variavelBoolean));
     
    float variavelFloat = -10;             
    System.out.println(String.valueOf(variavelFloat));
    
    int variavelInt = 9;
    System.out.println(String.valueOf(variavelInt));
     
    double variaelDouble = 10.30;
    System.out.println(String.valueOf(variaelDouble));
  }

}

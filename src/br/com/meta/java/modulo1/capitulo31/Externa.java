package br.com.meta.java.modulo1.capitulo31;

/**
 * Classe aninhada
 * @author heitormaciel
 *
 */

public class Externa {
	private String texto = "text externo";
	
	public Externa() {
			
	}
	
	public class Interna {
		private String texto = "texto interno";
		
		public Interna() {
			
		}
		
		public void imprimirTexto() {
			System.out.println(this.texto);
		}
		
		public void imprimirAmbosTextos() {
			System.out.println(this.texto);
			System.out.println(Externa.this.texto);
		}
	}
}

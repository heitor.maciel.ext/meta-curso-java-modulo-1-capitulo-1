package br.com.meta.java.modulo1.capitulo31;

import br.com.meta.java.modulo1.capitulo31.Externa.Interna;

public class TesteClassesAninhadas {

	public static void main(String[] args) {
		Externa externa = new Externa();
		
		Interna interna1 = externa.new Interna();
		// ou entao
		Interna interna = new Externa().new Interna();
		interna.imprimirTexto();
		//interna.imprimirAmbosTextos();
	}
}

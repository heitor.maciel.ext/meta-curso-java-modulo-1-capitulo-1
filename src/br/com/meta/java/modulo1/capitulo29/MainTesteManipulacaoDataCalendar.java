package br.com.meta.java.modulo1.capitulo29;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class MainTesteManipulacaoDataCalendar {

	public static void main(String[] args) {
		Calendar calendario = Calendar.getInstance();
		
		int ano = calendario.get(Calendar.YEAR);
		int mes = calendario.get(Calendar.MONTH);
		
		System.out.println(mes);
		
		calendario.add(Calendar.MONTH, 1);
		
		int ano2 = calendario.get(Calendar.YEAR);
		int mes2 = calendario.get(Calendar.MONTH);
		System.out.println(mes2);
		
		// ou entao
		
		calendario.add(Calendar.YEAR, -11);
		int ano3 = calendario.get(Calendar.YEAR);
		System.out.println(ano3);
		
		Calendar calendario1 = new GregorianCalendar(2019, 03, 01);
		Calendar calendario2 = new GregorianCalendar(2019, 01, 01);
		
		
		int dia1 = calendario1.get(Calendar.DAY_OF_YEAR);
		int dia2 = calendario2.get(Calendar.DAY_OF_YEAR);
		
		System.out.println("Diferença de dias: " + (dia1 - dia2));
		
	}

}

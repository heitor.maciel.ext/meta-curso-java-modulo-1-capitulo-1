package br.com.meta.java.modulo1.capitulo29;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class MainFormatandoDataSimpleDateFormat {

	public static void main(String[] args) {
		//https://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html
		
		//SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/y");
		
		//SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss Z");
		
		Calendar calendario = new GregorianCalendar(2010, 4, 5, 15, 44, 25);
		
		System.out.println(calendario.getTime());
		
		Locale.setDefault(Locale.ENGLISH);
		
		Calendar calendario2 = new GregorianCalendar(2019, 6, 15, 15, 44, 25);
		
		System.out.println(calendario2.getTime());
		
		
		
		// ou ao contrario
		
		/*
		 * SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy"); String data =
		 * "16/04/1992";
		 * 
		 * 
		 * 
		 * try { Date dataConvertida = sdf1.parse(data);
		 * System.out.println(dataConvertida); } catch (ParseException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 */
	}

}

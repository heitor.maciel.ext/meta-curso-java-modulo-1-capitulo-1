package br.com.meta.java.modulo1.capitulo29;

import java.util.Date;

public class MainTesteDate {

	public static void main(String[] args) {	
		Date hoje = new Date();
		System.out.println(hoje);
		
		/**
		 * Retornar o dia atual
		 */
		System.out.println(hoje.getDate());
		
		/**
		 * Retornar a hora atual
		 */
		System.out.println(hoje.getHours());
		
		/**
		 * Retornar os minutos
		 */
		System.out.println(hoje.getMinutes());
		
		/**
		 * Retorna o mês atual
		 */
		System.out.println(hoje.getMonth());
		
		/**
		 * Retorna os segundos
		 */
		System.out.println(hoje.getSeconds());
	}
}

package br.com.meta.java.modulo1.capitulo29;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class MainTesteGregorianCalendar {

	public static void main(String[] args) {
		/*
		 * Calendar calendario = Calendar.getInstance(); GregorianCalendar calendario2 =
		 * new GregorianCalendar();
		 * 
		 * System.out.println(calendario); System.out.println(calendario2);
		 * 
		 * System.out.println(calendario2.isLeapYear(2016));
		 * 
		 * imprimirData(calendario2);
		 * 
		 * Calendar calendario3 = Calendar.getInstance();
		 * 
		 * imprimirData(calendario3);
		 */
		
		GregorianCalendar calendario4 = new GregorianCalendar(2011, 1, 25);
		imprimirData(calendario4);
	}
	
	private static void imprimirData(Calendar calendario) { 
		int ano = calendario.get(Calendar.YEAR);
		int mes = calendario.get(Calendar.MONTH);
		int dia = calendario.get(Calendar.DAY_OF_MONTH);
		int hora = calendario.get(Calendar.HOUR_OF_DAY);
		int minutos = calendario.get(Calendar.MINUTE);
		int segundos = calendario.get(Calendar.SECOND);
		
		System.out.printf("O dia informado é: %02d/%02d/%d %02d:%02d:%02d",
				dia, mes, ano, hora, minutos, segundos
		);
		
		System.out.println(System.lineSeparator());
	}

}

package br.com.meta.java.modulo1.capitulo29;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

public class MainTesteDateFormat {

	public static void main(String[] args) {
		Date data = new Date();
		System.out.println(data);
		
		String hojeFormatado = DateFormat.getInstance().format(data);
		System.out.println(hojeFormatado);
		
		Locale.setDefault(Locale.ENGLISH);
		hojeFormatado = DateFormat.getInstance().format(data);
		System.out.println(hojeFormatado);
		
		Locale.setDefault(Locale.CHINA);
		hojeFormatado = DateFormat.getInstance().format(data);
		System.out.println(hojeFormatado);
		
		//getTimeInstance
		//getDateInstance
		//getDateTimeInstance
		//
	}

}

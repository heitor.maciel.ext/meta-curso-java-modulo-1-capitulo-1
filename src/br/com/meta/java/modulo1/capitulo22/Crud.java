package br.com.meta.java.modulo1.capitulo22;

public class Crud {
  ICrud interfaceCrud;
  
  Crud() {
    
  }
  
  Crud(ICrud interfaceCrud) {
    this.interfaceCrud = interfaceCrud;
  }
  
  public void generateCrud() {
    String nomeTabela = interfaceCrud.nomeTabela();
    
    System.out.println("Gerando o crud da tabela: " + nomeTabela);
  }
}

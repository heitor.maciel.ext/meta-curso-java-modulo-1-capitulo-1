package br.com.meta.java.modulo1.capitulo22;

public class TesteEnviaEmailPJ {

  public static void main(String[] args) {
    // Tela pessoa jurídica
    EnviaEmail email = new EnviaEmail(new IEmail() {
      @Override
      public void antesDoEmail() {
        System.out.println("Consultar o CNPJ");
      }
      @Override
      public void depoisDoEmail() {
        System.out.println("Cliente PJ cadastrado com sucesso.");
      }
    });
    email.setAssunto("Email agendamento sasacar");
    email.setEmailDestinatario("sascar@sascar.com.br");
    email.setNomeDestinatario("Heitor Maciel");
    email.setEmailRemetente("sascar@sascar.com.br");
    email.setNomeRemetente("");
    email.setPorta(587);
    email.setHost("10.1.029");
    email.setConteudo("Olá, seja bem vindo a sasacar");
    email.enviarEmailInterface();
  }

}

package br.com.meta.java.modulo1.capitulo22;

public class EnviaEmail {
  private String emailDestinatario;
  private String nomeDestinatario;
  private String emailRemetente;
  private String nomeRemetente;
  private Integer porta;
  private String host;
  private String assunto;
  private String conteudo;
  private IEmail interfaceEmail;
  
  public EnviaEmail() {

  }

  public EnviaEmail(IEmail interfaceEmail) {
    this.interfaceEmail = interfaceEmail;
  }
  
  public void enviar() {
    System.out.println("Envio do e-mail");
  }
  
  public void enviarEmailInterface() {
    this.interfaceEmail.antesDoEmail();
    enviar();
    this.interfaceEmail.depoisDoEmail();    
  }
  
  public String getEmailDestinatario() {
    return emailDestinatario;
  }

  public void setEmailDestinatario(String emailDestinatario) {
    this.emailDestinatario = emailDestinatario;
  }

  public String getNomeDestinatario() {
    return nomeDestinatario;
  }

  public void setNomeDestinatario(String nomeDestinatario) {
    this.nomeDestinatario = nomeDestinatario;
  }

  public String getEmailRemetente() {
    return emailRemetente;
  }

  public void setEmailRemetente(String emailRemetente) {
    this.emailRemetente = emailRemetente;
  }

  public String getNomeRemetente() {
    return nomeRemetente;
  }

  public void setNomeRemetente(String nomeRemetente) {
    this.nomeRemetente = nomeRemetente;
  }

  public String getHost() {
    return host;
  }

  public void setHost(String host) {
    this.host = host;
  }

  public String getAssunto() {
    return assunto;
  }

  public void setAssunto(String assunto) {
    this.assunto = assunto;
  }

  public String getConteudo() {
    return conteudo;
  }

  public void setConteudo(String conteudo) {
    this.conteudo = conteudo;
  }

  public Integer getPorta() {
    return porta;
  }

  public void setPorta(Integer porta) {
    this.porta = porta;
  }
}

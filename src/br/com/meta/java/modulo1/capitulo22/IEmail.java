package br.com.meta.java.modulo1.capitulo22;

public interface IEmail {
  void antesDoEmail();
  void depoisDoEmail();
}

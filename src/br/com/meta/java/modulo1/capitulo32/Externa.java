package br.com.meta.java.modulo1.capitulo32;

/**
 * Classe local
 * @author heitormaciel
 *
 */

public class Externa {
	
	public void metodoQualquer() {
		
		class ClasseLocal {
			private String texto = "texto interno classe local";
			
			public ClasseLocal() {
				
			}
			
			public void imprimeTexto() {
				System.out.println(this.texto);
			}
		}
		
		ClasseLocal local = new ClasseLocal();
		local.imprimeTexto();
	}
}

package br.com.meta.java.modulo1.aula4;

import java.util.Scanner;

public class Aula4 {
  /**
   * Lendo informações digitadas pelo usuário
   * @param args
   */
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    
    System.out.println("Informe o nome completo:");
    String nomeCompleto = scan.nextLine();
    
    System.out.println("Informe a sua idade:");
    String idade = scan.nextLine();
    
    System.out.println("Informe sua cidade:");
    String cidade = scan.nextLine();
    
    System.out.println("O nome digitado é: " + nomeCompleto);
    System.out.println("A idade digitada é: " + idade);
    System.out.println("A cidade digitada é: " + cidade);
  }
}

package br.com.meta.java.modulo1.aula3;

import java.math.BigDecimal;

public class Aula3 {
  
  /**
   * Aula de introdução a variáveis
   * @param args
   */
  public static void main(String[] args) {
    int idade;
    int idadePredio = 20;
    Integer idadePessoa;
    
    String nomePessoa;
    String nomeVeiculo = "Ford focus";
    
    boolean acimaVelocidade = true;
    Boolean abaixoVelocidade = false;
    
    float valorPrestacao = (float) 200.98;
    float valorPrestacao2 = 200.98f;
    Float vlrCondominio;
    
    double valorTenis = 500.98;
    Double valorAcademia;
    
    /**
     * Iremos falar mais pra frente destes assuntos
     */
    BigDecimal valorEmObjeto;
    
    byte caracter;
    byte[] imagem;
    
    char letraO = 111;
    char letraI = 105;
    
    System.out.println(letraO + letraI);
    System.out.println("" + letraO + letraI);
  }

}

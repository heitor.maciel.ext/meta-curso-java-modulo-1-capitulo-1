package br.com.meta.java.modulo1.capitulo27;

public interface ISQLDDL {
  void create(String Comando);
  void drop(String Comando);
  void alter(String Comando);
}

package br.com.meta.java.modulo1.capitulo27;

public interface IBancoDados extends ISQLDCL, ISQLDDL, ISQLDML {
  void abrirConexao();
  void fecharConexao();
}

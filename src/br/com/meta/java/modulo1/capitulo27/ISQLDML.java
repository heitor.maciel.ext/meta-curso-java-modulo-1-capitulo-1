package br.com.meta.java.modulo1.capitulo27;

public interface ISQLDML {
  void insert(String Comando);
  void update(String Comando);
  void delete(String Comando);
  String select(String Comando);
}

package br.com.meta.java.modulo1.capitulo27;

public interface ISQLDCL {
  void grant(String comando);
  void revoke(String comando);
}

package br.com.meta.java.modulo1.capitulo27;

public class BancoDados implements IBancoDados {

  @Override
  public void grant(String comando) {
    System.out.println("Realiza o comando grant");
  }

  @Override
  public void revoke(String comando) {
    System.out.println("Realiza o comando revoke");
    
  }

  @Override
  public void create(String Comando) {
    System.out.println("Realiza o comando create");
    
  }

  @Override
  public void drop(String Comando) {
    System.out.println("Realiza o comando drop");
    
  }

  @Override
  public void alter(String Comando) {
    System.out.println("Realiza o comando alter");
    
  }

  @Override
  public void insert(String Comando) {
    System.out.println("Realiza o comando insert");
    
  }

  @Override
  public void update(String Comando) {
    System.out.println("Realiza o comando update");
    
  }

  @Override
  public void delete(String Comando) {
    System.out.println("Realiza o comando delete");
    
  }

  @Override
  public String select(String Comando) {
    System.out.println("Realiza o comando select");
    return "Realiza o comando select";
  }

  @Override
  public void abrirConexao() {
    System.out.println("Realiza o comando abrirConexao");
    
  }

  @Override
  public void fecharConexao() {
    System.out.println("Realiza o comando fecharConexao");
    
  }

}

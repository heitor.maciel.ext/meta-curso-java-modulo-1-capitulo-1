# Meta - Curso Java - Modulo 1 - Capitulo 1

Seja bem vindo ao **Curso de Java** aplicado pelo **Grupo Meta**.

Este é o repositório do **Módulo 1 - Capitúlo 1** deste curso.

**Pré requisitos:**
*  Ja ter programado as menos em uma linguagem.
*  Ter um conhecimento básico em relação a orientação a objetos. (Obs: caso não tenha contato, não tem problema, vamos revisar os conceitos básicos)
*  Ter um básico em relação aos comandos do git. (Obs: caso não tenha contato, não tem problema, vamos revisar os comandos)

**Instalações na sua máquina:**
*  Git
*  Eclipse IDE
*  Java - Java SE Development Kit (A partir da versão 8)
*  

Contatos:
*  **WhatsApp:** (41) 99614-2896
*  **Microsoft Teams:** Heitor Maciel (heitor.maciel.ext)
*  **E-mail Sascar:** heitor.maciel.ext@sascar.com.br
*  **E-mail Meta:** heitor.maciel@meta.com.br

Att, Heitor Maciel